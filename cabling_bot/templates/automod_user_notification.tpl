A post you made in {{ channel }} on *{{ guild }}* has been automatically removed{% if message %}:
> {{ message }}{% endif %}

{{ reason }}{% if content_uri %}

See the server's policy on prohibited content: {{ content_uri }}{% endif %}

This incident has been reported to the server's staff. Feel free to reach out to one of them to discuss the aforementioned policy, or this incident in particular.