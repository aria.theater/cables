import distutils.util
from ast import literal_eval
from textwrap import dedent
from cabling_bot.bot import constants

from discord import (Object, Guild, Role, Member, User)
from typing import (Any, Union, Optional, List)


def add_fancy_quotes(
    text: str,
    italicize: bool = False
) -> str:
    return (constants.Symbols.ldquo
            + (f"*{text}*" if italicize else text)
            + constants.Symbols.rdquo)


def format_embed_footer(
    parts: list[str],
    linebreaks: bool = True
) -> str:
    footer = str()
    if linebreaks:
        footer += constants.Symbols.npc + "\n"
    footer += f"  {constants.Symbols.mid} ".join(parts)
    return footer


class HumanReadableBool():
    def __init__(self, category: str) -> None:
        self.category = category
        self.values = {
            "literal": ["false", "true"],
            "ability": ["disabled", "enabled"],
            "lightswitch": ["off", "on"]
        }
        self.map = {
            k: {(True if literal_eval(v) else False): v}
            for k, v in self.values.items()
        }

    def fromBool(self, value: bool) -> str:
        if self.category not in self.map.keys():
            allowed = str(list(self.map.keys()))
            e = f"Expected one of {allowed} for 'category' but got {self.category}"
            raise TypeError(e)
        if value not in self.map[self.category].keys():
            raise ValueError(f"Expected boolean for 'value', but got {value}")
        return self.map[self.category][value]


def bool_to_emoji(
    obj: bool,
    inverse: Optional[bool] = False
) -> str:
    true = constants.Emojis.confirm
    false = constants.Emojis.deny
    if not inverse:
        if obj:
            return true
        return false
    else:
        if obj:
            return false
        return true


def members_from_roles(
    guild: Guild,
    ids: List[Union[str, int]]
) -> List[Union[Member, User]]:
    if not ids:
        return False

    roles = [role for role in guild.roles if role.id in ids]
    return list(*map(lambda r: r.members, roles))


def truncate_text(text: str, limit=4096):
    """Truncate text to the desired character limit"""
    if text and len(text) >= limit:
        return text[: (limit - 4)] + "..."
    return text


def codeblock(msg: str):
    """Wrap a string in backticks to render as a codeblock"""
    return "```\n{}\n```".format(msg)


def cast_to_mention(
    candidate: Optional[Union[Role, Member, int, str]],
    guild: Guild,
) -> Optional[str]:
    """Cast something to the string representation of a Discord mention,
    where that something could be a Role, Member, integer ID, or role name."""
    if not candidate:
        return None

    if not isinstance(candidate, (Role, Member)):
        _object: Object = None
        if not isinstance(_object, Role):
            _object = guild.get_role(candidate)
        elif not isinstance(_object, (Role, Member)):
            _object = guild.get_member(candidate)

        if _object:
            return _object.mention
        elif isinstance(candidate, int) and not _object:
            return candidate
        elif isinstance(candidate, str):
            return f"@{candidate}"

        err = "Argument must be one of: [Role, Role.id, Role.name, Member, Member.id]"
        raise ValueError(candidate, err)

    return candidate.mention


def strtobool(s: str) -> bool:
    """Return boolean conversion of string, e.g. "yes" -> True"""
    return bool(distutils.util.strtobool(s))


def strtoliteral(s: str) -> Any:
    # boolean-like
    try:
        return strtobool(s)
    except (ValueError, AttributeError):
        pass
    # literals
    try:
        return literal_eval(s)
    except (ValueError, SyntaxError):
        pass
    return s


def clean_message(s: str) -> str:
    """Takes a multi-line string, and returns it \
    without indentation and with line breaks replaced with spaces.
    """
    if type(s) is not str:
        return s
    return dedent(s).replace("\n", " ")
