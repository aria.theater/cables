import asyncio
from typing import (Any, Awaitable, Coroutine)
from cabling_bot.bot import CablesBot as Bot


class QueueManager:
    """Event-based FIFO queue for asynchronous coroutine calls."""

    def __init__(self, bot: Bot) -> None:
        self._bot = bot
        self.buckets: dict[str, list] = {}

    def create_bucket(self, name: str) -> None:
        """Create an empty bucket within the intiialized queue."""
        if name not in self.buckets.keys():
            self.buckets[name] = []

    def put(self, name: str, coro: Coroutine, *args, **kwargs) -> None:
        """Put a coroutine to a queue bucket and dispatch an event."""
        self.create_bucket(name)
        self.buckets[name] += [(coro(*args, **kwargs))]
        self._bot.dispatch("queue_put", bucket=name)

    async def consume(self, name: str) -> Any:
        """Serially consume all coroutines in a queue bucket."""
        if name not in self.buckets:
            raise KeyError(f"Queue bucket '{name}' not found")
        while self.buckets[name]:
            task = self.buckets[name].pop(0)
            if isinstance(task, Awaitable):
                await task
            else:
                task

    async def wait_until_empty(self, name: str) -> None:
        """Block until a queue bucket has been fully processed."""
        self.create_bucket(name)
        while self.buckets[name]:
            await asyncio.sleep(0.1)
