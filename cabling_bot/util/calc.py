def rounded_division(denominator: float, dividend: float) -> int:
    return int(round(denominator / dividend, 0))


def ratio(a: int, b: int) -> int:
    raw = float(a / b)
    percent = raw * 100
    rounded = round(percent, 0)
    return int(rounded)
