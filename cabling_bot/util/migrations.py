from __future__ import annotations
from typing import TYPE_CHECKING

import logging

from typing import Optional
from discord import Guild
from psycopg import DatabaseError
from psycopg.types.json import Json as PostgresJson

if TYPE_CHECKING:
    from cabling_bot.bot import CablesBot as Bot

logger = logging.getLogger(__name__)


class MigrationManager():
    def __init__(self, bot: Bot):
        self._bot = bot
        self._db = bot.db
        self.revs = [
            "0.8.24",
            "0.8.32"
        ]

    def v0_8_32(
        self,
        guild: Guild,
        name: str,
        state: bool,
        config: Optional[dict],
    ) -> tuple[list, int]:
        failures, successes = [], 0

        q = """
            SELECT name FROM guild_cron_jobs
            WHERE guild = %(guild)s AND name = %(name)s
        """
        with self._bot.db.connection() as conn:
            cur = conn.execute(q, dict(guild=guild.id, name=name))
            if cur.rowcount:
                return ([], None)

        q = """
            INSERT INTO guild_cron_jobs
                (guild, name, enabled, config)
            VALUES
                (%(guild)s, %(name)s, %(state)s, %(config)s)
        """
        if config:
            config = PostgresJson(config)
        v = dict(guild=guild.id, name=name, state=state, config=config)
        with self._bot.db.connection() as conn:
            log = {"GUILD_SETUP": {"guild": guild.id}}
            try:
                conn.execute(q, v)
            except DatabaseError as e:
                log["GUILD_SETUP"]["error"] = e
                logger.error(log)
                failures += [f"{guild.id}/db/{e.sqlstate}"]
            else:
                log["GUILD_SETUP"]["note"] = f"Created '{name}' cron config"
                logger.info(log)
                successes += 1

        return (failures, successes)

    def v0_8_24(
        self,
        guild: Guild,
        known: dict,
        code: int,
        invite: dict,
    ) -> tuple[list, int]:
        failures, successes = [], 0

        if invite["max_uses"] and not known[code]["max_uses"]:
            sql = """
                UPDATE guild_invite_codes
                SET max_uses = %s
                WHERE code = %s
            """
            values = (invite["max_uses"], code)
            with self._db.connection() as conn:
                result = conn.execute(sql, values)
                if not result.rowcount:
                    failures += [f"{guild.id}/{code}/max_uses"]
                else:
                    successes += 1

        if invite["expires_at"] and not known[code]["expires_at"]:
            sql = """
                UPDATE guild_invite_codes
                SET expires_at = %s
                WHERE code = %s
            """
            values = (invite["expires_at"], code)
            with self._db.connection() as conn:
                result = conn.execute(sql, values)
                if not result.rowcount:
                    failures += [f"{guild.id}/{code}/expires_at"]
                else:
                    successes += 1

        return (failures, successes)
