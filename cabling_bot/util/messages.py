from __future__ import annotations
from typing import TYPE_CHECKING

from typing import (Optional, Union)
from discord import (
    Guild, HTTPException, TextChannel, VoiceChannel, Message,
    NotFound, Forbidden,
)
from discord.abc import PrivateChannel

from ..util import matching
from ..bot.constants import Emojis
from ..bot.errors import report_guild_error

if TYPE_CHECKING:
    from ..bot import (
        CablesBot as Bot,
        CablesContext as Context,
        CablesAppContext as ApplicationContext,
    )


async def get_message(
    bot: Bot,
    guild: Optional[Union[Guild, int]],
    channel_id: Optional[int],
    message_id: Optional[int]
) -> Optional[Message]:
    if not (guild and channel_id and message_id):
        return None

    if isinstance(guild, int) and not (guild := bot.get_guild(guild)):
        return None

    channel = guild.get_channel(channel_id)
    channel_types = (TextChannel, VoiceChannel, PrivateChannel)
    if not isinstance(channel, channel_types):
        return None

    try:
        msg = await channel.fetch_message(message_id)
    except (Forbidden, TypeError, HTTPException):
        return None
    return msg


def has_media(msg: Message) -> bool:
    has_content_links = matching.extract_all_urls(msg.content)
    has_media_embed = [
        e for e in msg.embeds if (
            e.type in ('image', 'video')
            or (e.type == 'rich'
                and (e.image or e.video))
        )
    ]
    return (
        (has_content_links and has_media_embed)
        or msg.attachments
    )


def has_text_link(msg: Message) -> bool:
    has_content_links = matching.extract_all_urls(msg.content)
    has_link_embed = [
        e for e in msg.embeds if (
            e.type == 'article'
            or (e.type == 'rich'
                and not (e.image or e.video))
        )
    ]
    return (
        has_content_links
        and (not msg.embeds or has_link_embed)
    )


async def create_post(
    bot: Bot,
    guild: Guild, channel_id: int,
    ctx: Optional[ApplicationContext] = None
) -> Optional[Message]:
    try:
        channel = await guild.fetch_channel(channel_id)
        return await channel.send(Emojis.hourglass)
    except (NotFound, Forbidden):
        report_text = ("Lacking permissions to post stats message "
                       + f"in {channel.mention}")
        user_text = f"{Emojis.deny} Post channel is inaccessible!"
        await report_guild_error(
            bot, ctx, None, guild,
            report_text, user_text=user_text
        )
        raise ValueError


async def reply_disapprovingly(
    destination: Union[PrivateChannel, TextChannel],
    mention: Optional[str] = None,
    text: Optional[str] = "",
) -> None:
    await destination.send(
        f"{mention} {Emojis.no_gesture} {text}",
        delete_after=5
    )
