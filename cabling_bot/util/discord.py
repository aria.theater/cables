from __future__ import annotations
from typing import TYPE_CHECKING

import logging
import requests
from collections import OrderedDict
from discord import (Member, Status, AuditLogAction)
from discord.errors import (Forbidden, HTTPException, NotFound)
from discord.abc import GuildChannel
from discord.utils import find
from ..bot import constants

if TYPE_CHECKING:
    from typing import (Union, Optional, Literal)
    from ..bot import (
        CablesContext as Context,
        CablesBot as Bot,
    )
    from discord import (
        Guild, Role, Invite, Widget, User,
        TextChannel, VoiceChannel, Thread
    )

logger = logging.getLogger(__name__)


def get_report_channel(
    bot: Bot, guild: Guild
) -> Union[TextChannel, VoiceChannel, Thread]:
    gid = guild.id
    channel = bot.get_guild_key(gid, "privileged", "mod_channel")
    if type(channel) is int:
        channel = guild.get_channel(channel)
    if not channel:
        channel = get_system_channel(bot, guild)
    return channel


def get_system_channel(bot: Bot, guild: Guild) -> Optional[TextChannel]:
    channel = bot.get_guild_key(guild.id, "privileged", "system_channel")
    if type(channel) is int:
        channel = guild.get_channel(channel)
    else:
        channel = guild.system_channel
    if not isinstance(channel, GuildChannel):
        raise NotFound
    return channel


def guild_attributes(
    guild: Guild,
    user: Optional[Union[Member, User]] = None
) -> Optional[dict]:
    attributes = OrderedDict()

    member = None
    if user and not isinstance(user, Member):
        member = guild.get_member(user.id)
        if member:
            attributes |= {
                "owned": (False, constants.Emojis.owner)[
                    member == guild.owner
                ],
                "administered": (False, constants.Emojis.administrator)[
                    (member.guild_permissions.administrator)
                    and (member != guild.owner)
                ],
            }

    attributes |= {
        "large": (False, constants.Emojis.mammoth)[
            guild.large
        ],
        "community": (False, constants.Emojis.neighborhood)[
            "COMMUNITY" in guild.features
        ],
        "verified": (False, constants.Emojis.confirm)[
            "VERIFIED" in guild.features
        ],
        "partnered": (False, constants.Emojis.knot)[
            "PARTNERED" in guild.features
        ],
        "monetized": (False, constants.Emojis.money_bag)[
            "ROLE_SUBSCRIPTIONS_AVAILABLE_FOR_PURCHASE" in guild.features
        ],
        "automod": (False, constants.Emojis.robot)[
            "AUTO_MODERATION" in guild.features
        ],
        "discoverable": (False, constants.Emojis.magnify)[
            "DISCOVERABLE" in guild.features
        ],
        "school": (False, constants.Emojis.backpack)[
            ("HUB" in guild.features)
            or ("LINKED_TO_HUB" in guild.features)
        ],
        "internal": (False, constants.Emojis.cop)[
            "INTERNAL_EMPLOYEE_ONLY" in guild.features
        ],
    }

    return attributes


def get_administrators(guild: Guild) -> list[Member]:
    return [
        m for m in guild.members
        if m.guild_permissions.administrator and not m.bot
    ]


def get_janitors(guild: Guild) -> list[Member]:
    return [
        m for m in guild.members if (
            m.guild_permissions.manage_roles
            or m.guild_permissions.manage_channels
            or m.guild_permissions.manage_permissions
            or m.guild_permissions.moderate_members
            or m.guild_permissions.manage_messages
        ) and (
            not m.bot
            and not m.guild_permissions.administrator
        )
    ]


def list_members(
    guild: Guild,
    statuses: Union[list[Status], constants.UserStatuses]
) -> list[Member]:
    return [
        m for m in guild.members if (
            m.status.name in statuses
            and not m.bot
        )
    ]


def get_member_breakdown(guild: Guild) -> tuple:
    bots = sum(member.bot for member in guild.members)
    humans = guild.member_count - bots
    ratio = round(bots / humans, 1)
    return (bots, humans, ratio)


def has_excessive_bots(
    bots: int,
    humans: int,
    ratio: float
) -> tuple:
    threshhold = constants.BotRatio.min
    if (bots + humans) > 50:
        threshhold = constants.BotRatio.max
    if ratio >= threshhold.value:
        return True
    return False


async def get_invitation(
    bot: Bot, guild: Guild,
    channel: Optional[TextChannel] = None,
    print_err: Optional[bool] = False
) -> Optional[Invite]:
    """Locates or creates a guild invitation.

    Looks for any bot-created or reusable invite on the given guild. \
        If one is found, it's sent. Otherwise, it tries to create one. \
        If `channel` is passed, it first ensures that channel is part \
        of the guild. Otherwise, the system channel, or the first text \
        channel encountered will be the destination for the invite.

    Returns the `Invite`, or `False.
    """
    invites, invite = await guild.invites(), None
    if invites:
        invite = find(
            lambda i: (
                i.inviter == bot.user
                or (not i.max_uses and i.inviter != bot.user)
            ), invites
        )

    if not invite:
        if not channel or channel.guild.id != guild.id:
            # NOTE: for whatever reason, even hitting the API doesn't
            # populate the cache: `guild.system_channel` is None >:(
            guild = await bot.fetch_guild(guild.id)
            channels = await guild.fetch_channels()
            channel = (
                guild.system_channel
                or find(lambda c: c.type.name == "text", channels)
            )
            if print_err and bot.environment != "production":
                err = "send_invitation: "
                logger.debug(err + f"Guild={repr(guild)}")
                logger.debug(err + f"Guild.system_channel={guild.system_channel}")
                logger.debug(err + f"Guild.fetch_channels()={channels}")
        try:
            invite: Invite = await channel.create_invite()
        except (AttributeError, HTTPException, Forbidden) as e:
            if print_err:
                logger.error(f"Failed to create invite: {e}")
            return False

    return invite


# for when bot isn't on the guild, but has the auth token
# e.g. guild.fetch_webook() isn't available
def check_webhook(ctx: Context, id: str, token: str) -> Optional[bool]:
    # TODO: add retry handling with exponential backoff
    response = requests.get(
        f"{ctx.bot.api_endpoint}/webhooks/{id}/{token}",
        timeout=10
    )

    http_status = response.status_code
    json: Optional[dict] = response.json()
    api_code = None
    if "code" in json.keys():
        api_code = json["code"]

    if http_status == 200:
        logger.debug(f"Webhook {id} accessible")
        return True
    elif (
        http_status == 404
        and api_code == 10015
    ):
        # unknown webhook
        logger.debug(f"Webhook {id} not found")
        return None
    elif (
        http_status == 401
        and api_code == 50027
    ):
        # invalid webhook token
        logger.debug(f"Webhook {id} unknown")
        return False
    else:
        return False


def find_role(guild: Guild, role: str) -> Optional[Role]:
    return find(lambda r: r.name == role, guild.roles)


async def _get_widget(
    guild: Guild
) -> Optional[Union[Widget, Literal[False]]]:
    """Returns a `Widget` object for a given `Guild`.

    If permissions are lacking, or the gateway returns an error, returns `False`.

    If the guild is known to be unavailable by the framework, returns `None`."""
    widget = None
    if not guild.unavailable:
        try:
            widget = await guild.widget()
        except (Forbidden, HTTPException):
            widget = False
    return widget


async def get_guild_settings(
    guild: Guild
) -> tuple[
    str, str, str, int,
    Optional[Union[Widget, Literal[False]]],
]:
    """Return a subset of the guild's admin-configurable settings as a `tuple`.
    - `Guild.default_notifications.name` (see `discord.NotificationLevel`)
    - `Guild.verification_level.name` (see `discord.VerificationLevel`)
    - `Guild.explicit_content_filter.name` (see `discord.ContentFilter`)
    - `Guild.mfa_level`
    - `Guild.widget | False | None` (see `util.discord._get_widget` and `discord.Widget`)
    """
    widget = None
    if not guild.unavailable:
        widget = await _get_widget(guild)
    return (
        guild.default_notifications.name,
        guild.verification_level.name,
        guild.explicit_content_filter.name,
        guild.mfa_level,
        widget,
    )


async def list_bans(guild: Guild) -> list:
    bans = list()
    try:
        async for b in guild.bans(limit=None):
            bans += [b]
    except (Forbidden, HTTPException):
        bans.clear()
    return bans


async def list_kicks(guild: Guild) -> list:
    kicks = list()
    try:
        async for entry in guild.audit_logs(
            action=AuditLogAction.kick,
            limit=None
        ):
            kicks += [entry.target]
    except (Forbidden, HTTPException):
        kicks.clear()
    return kicks
