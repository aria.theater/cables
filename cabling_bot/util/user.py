from __future__ import annotations
from typing import TYPE_CHECKING

import logging
import discord.utils
from dateutil.relativedelta import relativedelta
from ..bot import constants
from .files import (checksum_file, delete_file)

from typing import (Optional, Union, NamedTuple)
from discord import (Guild, User, Member, Forbidden, NotFound)

if TYPE_CHECKING:
    from ..bot import CablesBot as Bot

logger = logging.getLogger(__name__)


# props to dredd-bot/Dredd for some of these
# https://github.com/dredd-bot/Dredd/blob/master/utils/publicflags.py
def collect_badges(user: Union[Member, User]) -> list:
    flags, badges = user.public_flags, []

    if flags.staff:
        badges.append("🧑‍💼")  # 🤬

    if flags.value & constants.UserFlags.spammer:
        badges.append("👺")

    if flags.discord_certified_moderator:
        badges.append("🧑‍⚖️")  # 🙄
    if (
        flags.partner
        or flags.value & constants.UserFlags.partnership_pending
    ):
        badges.append("🪢")  # 👼

    if (
        flags.bug_hunter
        or flags.bug_hunter_level_2
    ):
        badges.append("🧑‍🔧")  # 🕵

    if (
        flags.early_verified_bot_developer
        or flags.verified_bot_developer
    ):
        badges.append("🧑‍💻")
    if (
        user.bot
        or flags.verified_bot
        or flags.team_user
    ):
        badges.append("🤖")

    if (
        flags.early_supporter
        # or user.premium_type.nitro_classic
        # or user.premium_type.nitro
    ):
        badges.append(constants.Emojis.nitro)
    elif flags.value & constants.UserFlags.nitro_disabled:
        badges.append(constants.Emojis.nitro_disabled)

    if flags.hypesquad:  # HypeSquad Events
        badges.append("🍾")
    if (
        flags.hypesquad_brilliance
        or flags.hypesquad_bravery
        or flags.hypesquad_balance
    ):
        badges.append(constants.Emojis.hypesquad)

    return badges


def account_age_check(
    bot: Bot,
    user: Union[Member, User],
    guild: Optional[Guild]
) -> bool:
    threshold = None
    if guild:
        threshold = bot.get_guild_key(
            guild.id,
            "notifications",
            "account_age_threshold"
        )
    if not threshold:
        threshold = relativedelta(months=+1)
    if not isinstance(threshold, relativedelta):
        threshold = relativedelta(weeks=threshold)

    now = discord.utils.utcnow()
    if user.created_at > (now - threshold):
        return False

    return True


async def can_send_dm(
    target: Union[Member, User],
    bot_user: User
) -> Optional[bool]:
    """Hackily check if a `User` can be DM'd successfully.

    Workaround for `discord.Object.can_send()` always returning `True`:
    https://github.com/Pycord-Development/pycord/blob/06ac55b/discord/abc.py#L1651-L1659

    Checks for mutual guilds and message history that can be reacted to.
    - Returns `False` when `user` resolves to `None`.
    - Returns `False` when `Forbidden` raised when trying to react.
    - Returns `None` when no shared guilds, nor prior DM history."""

    if not target:
        raise ValueError("can_send_dm: user arg cannot be None")
    if not isinstance(target, (User, Member)):
        raise TypeError("can_send_dm: user arg accepts types: [User, Member]")

    history = False
    async for m in target.history(limit=1, oldest_first=False):
        if m:
            history = m

    if history:
        try:
            await m.add_reaction(constants.Emojis.speech_bubble)
        except Forbidden:
            return False
        else:
            await m.remove_reaction(
                constants.Emojis.speech_bubble,
                bot_user
            )
            return True

    if not target.mutual_guilds:
        return None


async def fetch_member(guild: Guild, member: Member) -> Optional[Member]:
    try:
        return await guild.fetch_member(member)
    except NotFound:
        return None


class CustomUser(NamedTuple):
    user: Optional[Union[Member, User]]
    name: Union[int, str]
    mention: str


async def safe_get(
    bot: Bot,
    user_id: int,
    guild: Optional[Guild] = None
) -> CustomUser:
    user = None
    if guild:
        user = guild.get_member(user_id)
    if not user:
        user = bot.get_user(user_id)
    if not user:
        try:
            user = await bot.fetch_user(user_id)
        except NotFound:
            # self._remove(guild, user, 0)
            user = user_id

    if type(user) is int:
        return CustomUser(None, user, "Inaccessible Account")
    else:
        return CustomUser(user, user.name, user.mention)


async def has_default_avatar(
    bot: Bot,
    user: Union[Member, User]
) -> bool:
    no_pfp = (not user.avatar)

    hash = None
    if not no_pfp:
        path = f"{bot.tmp_path}/{user.avatar.key}"
        await user.avatar.save(path)
        hash = checksum_file(path)
        delete_file(path)

    if no_pfp or hash in constants.DefaultAvatars:
        return True
    return False
