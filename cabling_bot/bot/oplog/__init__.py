import logging
import inspect
from typing import Awaitable, Optional
from discord import Object
from .messages import MessagesComponent

logger = logging.getLogger(__name__)


class OpLog():
    """Console log line composer."""

    def __init__(self, bot):
        self.component = self.Components(bot)

    class Components():
        def __init__(self, bot):
            self.messages = MessagesComponent(bot)

        @property
        def messages(self) -> MessagesComponent:
            return self._messages

        @messages.setter
        def messages(self, value):
            self._messages = value

    async def compose(
        self,
        component: str,
        method: str,
        **kwargs: Optional[Object]
    ) -> dict:
        cmp_ref = getattr(self.component, component)
        coro = getattr(cmp_ref, method)
        if inspect.isawaitable(coro):
            result = await coro(**kwargs)
        else:
            result = coro(**kwargs)

        if not result:
            logger.error(f"Unexpected response from {component}.{method}()")
        return result
