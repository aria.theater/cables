from discord.ext.commands import CheckFailure
from pkg_resources import cleanup_resources


class MemberRoleInferior(CheckFailure):
    pass


class BotRoleInferior(CheckFailure):
    pass


class GmodsOnlyError(CheckFailure):
    pass


class ChanopsOnlyError(CheckFailure):
    pass


class StaffOnlyError(CheckFailure):
    pass


class AuditReasonRequired(CheckFailure):
    pass


class AuditReasonOverloaded(CheckFailure):
    pass


class DefconConstraintViolation(CheckFailure):
    pass


class DMsDisallowed(CheckFailure):
    pass


class GuildMembershipError(CheckFailure):
    pass


class GuildUnavailable(CheckFailure):
    pass


class GuildOwnerOnlyError(CheckFailure):
    pass


class FeatureDisabled(CheckFailure):
    pass


class RceDisabled(CheckFailure):
    pass


class GuildChannelNotFound(CheckFailure):
    pass


class GuildChannelInaccessible(CheckFailure):
    pass


class GuildChannelUnscannable(CheckFailure):
    pass


class GuildChannelUnwritable(CheckFailure):
    pass


class ThreadUnwritable(CheckFailure):
    pass


class ThreadArchived(CheckFailure):
    pass


class GuildMessageNotFound(CheckFailure):
    pass


class GuildInvitesPaused(CheckFailure):
    pass
