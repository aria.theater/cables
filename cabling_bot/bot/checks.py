import logging
import json
import discord.utils

from typing import (Iterable, Literal, Optional, Union)
from discord.abc import GuildChannel
from discord import (Guild, Role, Message, TextChannel, Thread, VoiceChannel)
from discord.errors import (CheckFailure, Forbidden, InvalidArgument, NotFound)
from discord.ext.commands.errors import RoleNotFound
from discord.ext.commands.converter import PartialMessageConverter
from ..bot import exceptions as CablesException
from ..bot import (
    CablesContext as Context,
    CablesAppContext as ApplicationContext
)

from discord.ext.commands import check
from ..util.privileged import (
    list_owners,
    list_administrators,
    list_global_moderators,
    is_channel_moderator
)


logger = logging.getLogger(__name__)


def is_feature_enabled(feature: str) -> bool:
    async def predicate(ctx: ApplicationContext | Context) -> bool:
        gid = ctx.guild.id
        if not ctx.get_guild_key(gid, "feature_flags", feature):
            raise CablesException.FeatureDisabled
        return True
    return check(predicate)


def is_rce_enabled() -> bool:
    async def predicate(ctx: Context):
        _enabled = bool(
            ctx.bot.get_config("rce_exploit", False)
            and ctx.bot.get_config("clown_mode", False)
        )
        if not _enabled or ctx.bot.environment == "production":
            raise CablesException.RceDisabled
        return True
    return check(predicate)


def _is_app_owner(ctx: Context) -> bool:
    return ctx.author.id == ctx.bot.operator.id


def is_app_owner() -> bool:
    """Returns truthy only when the requestor is that who created the bot application.

    This differs from the built-in `is_owner()` check, which returns truthy for:
    - Any user listed in `Bot.owner_ids`, inclusive of the user who created the bot application`
    - Any application tester as configured from the Discord Developer panel for the application
    """
    async def predicate(ctx: Context):
        if not _is_app_owner(ctx):
            raise CablesException.GuildOwnerOnlyError
        return True
    return check(predicate)


def is_dev_guild_reconfig() -> bool:
    async def predicate(ctx: Context):
        if not ctx.bot.dev_guild:
            return None
        else:
            return (
                ctx.bot.dev_guild.id == ctx.guild.id
                and _is_app_owner(ctx)
            )
    return check(predicate)


def is_guild_owner() -> bool:
    """Returns True for callers with rights of an owner.

    See `util.privileged.list_owners()`"""
    async def predicate(ctx: Union[ApplicationContext, Context]):
        owners = list()
        if (
            not (owners := list_owners(ctx, ctx.guild))
            and ctx.get_guild_key(ctx.guild.id, "defcon", "ownerless_safety")
        ):
            raise CablesException.DefconConstraintViolation
        if ctx.author not in owners:
            raise CablesException.GuildOwnerOnlyError
        return True
    return check(predicate)


def is_admin() -> bool:
    """Returns True for callers with rights of an Administrator or higher.

    See `util.privileged.list_administrators()`"""
    async def predicate(ctx: Union[ApplicationContext, Context]):
        admins = list_administrators(ctx, ctx.guild)
        if ctx.author not in admins:
            raise CablesException.StaffOnlyError
        return True
    return check(predicate)


def is_global_mod() -> bool:
    """Returns True for callers with rights of a Global Moderator.

    See `util.privileged.list_global_moderators()`"""
    async def predicate(ctx: Union[ApplicationContext, Context]):
        global_mods = list_global_moderators(ctx, ctx.guild)
        if ctx.author not in global_mods:
            raise CablesException.GmodsOnlyError
        return True
    return check(predicate)


def is_channel_mod() -> bool:
    """Returns True for callers with rights of a Channel Moderator.

    See `util.privileged.list_channel_moderators()`"""
    async def predicate(ctx: Union[ApplicationContext, Context]):
        if not is_channel_moderator(ctx):
            raise CablesException.ChanopsOnlyError
        return True
    return check(predicate)


def is_staff_member() -> bool:
    """Returns True for callers with rights of any staff member class.

    Specifically, this checks the user's roles for that defined by `privileged.staff_role`"""
    async def predicate(ctx: Union[ApplicationContext, Context]):
        role = ctx.bot.get_privileged_role("staff", ctx.guild)
        if (
            (role and role not in ctx.author.roles)
            or ctx.author.id == ctx.guild.owner.id
        ):
            raise CablesException.StaffOnlyError
        return True
    return check(predicate)


def is_role_superior() -> bool:
    async def predicate(ctx: Context):
        _content = ctx.message.clean_content.split(" ")
        cmd = _content[0].strip(ctx.bot.command_prefix)
        cmd_args = _content[1:]
        if cmd == "help":
            return True

        # examples:
        #   ['^role' 'add' 1234567890 'userid' ...]
        #   ['^role' 'add' 'rolename' 'userid' ...]
        #   ['^role' 'add' '"role' 'name"' 'userid' ...]
        try:
            proposed_role: Role = ctx.message.role_mentions[0]
        except IndexError:
            # assume cmd has a subcommand 😩
            role_name = cmd_args[1]
            # detect multi-word role names
            if role_name.startswith("\""):
                role_name = ctx.message.content.split("\"")[1]

            # get role object from name
            proposed_role: Role = discord.utils.find(
                lambda r: r.name == role_name, ctx.guild.roles
            )
            # NOTE: are there performance differences between `find` and `get`?
            # proposed_role: Role = discord.utils.get(
            #     ctx.guild.roles,
            #     name=role_name
            # )

        if not proposed_role:
            raise RoleNotFound(role_name)

        # check given role against bot's highest role
        if proposed_role > ctx.guild.me.top_role:
            raise CablesException.BotRoleInferior
        # check given role against command issuer's highest role
        elif proposed_role > ctx.author.top_role:
            raise CablesException.MemberRoleInferior

        return True

    return check(predicate)


def is_guild_invitable() -> bool:
    async def predicate(ctx: ApplicationContext):
        if "COMMUNITY" in ctx.guild.features:
            if not getattr(ctx.guild, "invites_disabled"):
                if "INVITES_DISABLED" in ctx.guild.features:
                    raise CablesException.GuildInvitesPaused
                else:
                    return True
            if ctx.guild.invites_disabled:
                raise CablesException.GuildInvitesPaused
            else:
                return True
        else:
            return True
    return check(predicate)


def guild_in_purgatory() -> bool:
    async def predicate(ctx: Context):
        guild = ctx.message.guild
        try:
            owner = await guild.fetch_member(guild.owner.id)
        except NotFound:
            owner = None
        if (
            ctx.get_guild_key(ctx.guild.id, "defcon", "ownerless_safety")
            and not owner
        ):
            raise CablesException.DefconConstraintViolation
        return True
    return check(predicate)


def check_sanction_reason() -> bool:
    """Requires a reason for the action be provided as the 2nd arg of a subcommand.

    Example: `ban add <user> <reason>`"""
    async def predicate(ctx: Context):
        msg = ctx.message.content
        if msg.startswith(f"{ctx.prefix}help"):
            return True

        enabled = ctx.bot.get_guild_key(
            ctx.guild.id,
            "moderation", "require_sanction_reasons"
        )
        if not enabled:
            return True

        fields = len(msg.split(" "))
        if fields < 4:
            raise CablesException.AuditReasonRequired

        reason = " ".join(msg.split(" ")[3:])
        audit = json.dumps(dict(issuer=ctx.author.id, text=reason))
        length = len(reason)
        max_length = 512 - (len(audit) - len(reason))
        if length > max_length:
            # NOTE: couldn't figure out how to pass this to the exception handler
            bounds = f"**{length}**/{max_length}"
            reply = f"Sorry, the reason must\\* be shorter! ({bounds})\n\n"
            reply += "\\*Audit log messages are limited to 512 characters, "
            reply += "but we use some of that for metadata."
            await ctx.fail_msg(reply)
            raise CablesException.AuditReasonOverloaded

        return True

    return check(predicate)


async def _find_guild_in_args(ctx: Context):
    args = ctx.message.content.split(" ")
    guild = None
    while len(args) > 2:
        arg = args.pop(2)
        if type(arg) is str:
            guild = discord.utils.get(ctx.bot.guilds, name=arg)
            if guild:
                break
        elif type(arg) is int:
            try:
                guild = await ctx.bot.fetch_guild(arg)
                break
            except NotFound:
                pass
        else:
            raise InvalidArgument
    if not guild:
        return None
    return guild


def has_guild_membership():
    """Require the requestor to be a member of the given guild.

    Bot owners are immune to this check."""
    async def predicate(ctx: Context):
        guild = await _find_guild_in_args(ctx)
        if (
            type(guild) is Guild
            and guild not in ctx.author.mutual_guilds
            and ctx.author.id not in ctx.bot.owner_ids
        ):
            raise CablesException.GuildMembershipError
        return True
    return check(predicate)


def is_guild_available():
    """Checks if a guild passed is available through the gateway."""
    async def predicate(ctx: Context):
        guild = await _find_guild_in_args(ctx)
        if not guild:
            guild = ctx.guild
        if guild.unavailable:
            raise CablesException.GuildUnavailable
        return True
    return check(predicate)


async def _check_channel_view_perms(
    ctx: Context,
    channel_id: int,
    checks: Iterable[Literal["view", "read", "write"]],
) -> Union[GuildChannel, Thread]:
    channel = None
    try:
        channel = await ctx.guild.fetch_channel(channel_id)
    except NotFound:
        raise CablesException.GuildChannelNotFound(str(channel_id))
    except Forbidden:
        if "view" in checks:
            raise CablesException.GuildChannelInaccessible(str(channel_id))
        else:
            pass

    return channel


def _check_channel_readwrite_perms(
    ctx: Context,
    channel: Union[Thread, GuildChannel],
    checks: Iterable[Literal["view", "read", "write"]],
) -> bool:
    e = str(channel.id), channel.name
    if isinstance(channel, Thread):
        parent_perms = channel.parent.permissions_for(ctx.guild.me)
        if (
            "write" in checks
            and not parent_perms.send_messages_in_threads
        ):
            raise CablesException.ThreadUnwritable(*e)
        elif (
            ("read" in checks or "write" in checks)
            and channel.archived
        ):
            # we throw this for read as MessageConverter would otherwise
            # throw discord.ext.commands.errors.ChannelNotFound`
            raise CablesException.ThreadArchived(*e)
    elif isinstance(channel, (TextChannel, VoiceChannel)):
        channel_perms = channel.permissions_for(ctx.guild.me)
        if "read" in checks and not channel_perms.read_message_history:
            raise CablesException.GuildChannelUnscannable(*e)
        elif "write" in checks and not channel_perms.send_messages:
            raise CablesException.GuildChannelUnwritable(*e)
    else:
        # unsupport channel type
        return False
    return True


async def _check_message_exists(
    channel: Union[GuildChannel, Thread],
    msg_id: int,
    checks: Iterable[Literal["view", "read", "write"]],
) -> Optional[Message]:
    try:
        msg = await channel.fetch_message(msg_id)
    except NotFound:
        if "view" in checks:
            e = str(channel.id), channel.name, msg_id
            raise CablesException.GuildMessageNotFound(*e)
        else:
            pass
    except Forbidden:
        if "read" in checks:
            e = str(channel.id), channel.name
            raise CablesException.GuildChannelInaccessible(*e)
        else:
            pass
    return msg


def validate_arg_permissions(
    checks: Iterable[Literal["view", "read", "write"]],
    index: int,
):
    async def predicate(ctx: Context) -> Union[
        Literal[False], GuildChannel, Thread, Message
    ]:
        err = "Programming error in `validate_arg_permissions`: "
        if not len(checks):
            err += "No `check` actions passed"
            raise CheckFailure(err)

        args = ctx.message.content.split(" ")
        if (index + 1) > len(args):
            err += "Invalid argument index"
            raise CheckFailure(err)
        arg = args.pop(index)  # [self,] ctx, arg, ...

        chid, mid = None, None
        if arg.startswith("http"):
            try:
                assert arg.endswith("/")
                gid, mid, chid = PartialMessageConverter._get_id_matches(ctx, arg)
                assert int(gid) and int(chid) and int(mid)
            except AssertionError:
                err += ("URI must be of a fully qualified message "
                        + "with all ID components!(PROTIP: For threads, "
                        + "use the channel reference format "
                        + "<#1234567890123456>)")
                raise CheckFailure(err)
        elif arg.startswith("<#"):
            chid = int(arg.removeprefix("<#").removesuffix(">"))

        if (
            not chid
            or (chid and (arg.startswith("http") and not mid))
        ):
            err += f"Index ({index}) must be of a channel or message, not '{arg}'"
            raise CheckFailure(err)

        channel = await _check_channel_view_perms(ctx, chid, checks)
        if not _check_channel_readwrite_perms(ctx, channel, checks):
            err += "Unsupported channel type"
            raise CheckFailure(err)

        if mid and ("read" in checks or "view" in checks):
            return await _check_message_exists(channel, mid, checks)
        return channel
    return check(predicate)
