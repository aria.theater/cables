from datetime import datetime
import logging
import types
import os
import os.path
import imp

from typing import (NamedTuple, Optional, Union, TypeVar)
from discord import (PartialMessageable, TextChannel, Thread, VoiceChannel)
from discord.abc import PrivateChannel

MessageableChannel = TypeVar(
    "MessageableChannel",
    bound="Union[TextChannel, VoiceChannel, Thread, PartialMessageable, PrivateChannel]"
)

logger = logging.getLogger(__name__)


class Response(NamedTuple):
    data: bool
    error: Optional[str] = None


class StateData(NamedTuple):
    state: bool
    data: datetime


# props to 'agf' for this
# https://stackoverflow.com/a/9865578
class PluginMeta(type):
    def __new__(cls, name, bases, dct):
        logger.debug(getattr(dct, "pluginfiles"))
        if getattr(dct, "plugindir"):
            modules = [
                imp.load_source(f, os.path.join(dct["plugindir"], f))
                for f in os.listdir(dct["plugindir"])
            ]
        elif getattr(dct, "pluginfiles"):
            modules = [
                imp.load_source(f, os.path.join(dct["pluginfiles"], f))
                for f in dct["pluginfiles"]
            ]

        for module in modules:
            for name in dir(module):
                function = getattr(module, name)
                if isinstance(function, types.FunctionType):
                    dct[function.__name__] = function

        return type.__new__(cls, name, bases, dct)
