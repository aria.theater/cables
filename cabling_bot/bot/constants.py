from enum import Enum
from discord import (Status, Guild)


CdnUri: str = "https://cdn.catgirl.technology"
CdnImagesPath: str = "/img"


class ServiceLimits():
    class BotUnverified():
        guild_limit: int = 100

    class Embed():
        title: int = 256
        description: int = 4096
        field_name: int = 256
        field_value: int = 1024
        footer: int = 2048
        author: int = 256
        sum: int = 6000

    class Guilds():
        # https://bestfriendsclub.ca/discord-member-limit/
        # https://www.quora.com/How-does-Discord-limit-the-amount-of-online-people-on-a-server/answers/171556496
        # https://tipsforefficiency.com/discord-voice-channel-limit/
        class Soft():
            online_members: int = 5000
            total_members: int = 25000

        class Hard():
            offline_presence: int = 1000
            categories: int = 50
            channels: int = 500
            roles: int = 250

            # see discord.gateway.identify()
            large_threshold: int = 250

        soft = Soft()
        hard = Hard()
        premium = Guild._PREMIUM_GUILD_LIMITS

    class Channels():
        name: int = 1000
        topic_default: int = 1024
        topic_forum: int = 4096
        vc_parts_audio: int = 99
        vc_parts_live: int = 10

    # represented in bytes
    class Assets():
        class PFP():
            filesize: int = (10 * (10**6))

        class Sticker():
            filesize: int = (512 * (10**3))

        class Emoji():
            filesize: int = (256 * (10 ** 3))

        class Attachment():
            # see ServiceLimits().guilds.premium[n].filesize
            description: int = 1024

        pfp = PFP()
        sticker = Sticker()
        emoji = Emoji()

    class Webhooks():
        # per webhook/channel
        # https://devforum.roblox.com/t/discord-webhook-limits/1436356/15
        posts_per_minute: int = 30

    bot_unverified = BotUnverified()
    embed = Embed()
    guilds = Guilds()
    channels = Channels()
    assets = Assets()

    message_content = 2048


class DevGuildTemplate():
    v1: str = "https://discord.new/uESRDzecSVaK"
    v2: str = "https://discord.new/s3VZtVYZRDSV"
    current = v2


class UserStatuses():
    available: list[str] = [Status.online.name]
    busy: list[str] = [Status.dnd.name, Status.streaming.name]
    present: list[str] = available + busy
    afk: list[str] = [Status.idle.name]
    online: list[str] = present + afk
    unknown: list[str] = [Status.offline.name]
    all: list[str] = online + unknown


class BotRatio(Enum):
    min = 1.7
    max = 1.1


class Colours():
    blue: int = 0x3775a8
    bright_green: int = 0x01d277
    orange: int = 0xe67e22
    pink: int = 0xcf84e0
    purple: int = 0xb734eb
    soft_green: int = 0x68c290
    dim_green = 0x589a77
    soft_orange: int = 0xf9cb54
    soft_red: int = 0xcd6d6d
    white: int = 0xfffffe
    yellow: int = 0xffd241
    greyblue: int = 0x7aa7bf
    steelblue: int = 0x4682b4
    lightsteelblue: int = 0xb0c4de
    deepskyblue: int = 0x00bfff
    wizzypurple: int = 0xaea8d3
    barely_red: int = 0xbf9f9d
    role_red: int = 0x855d5d
    degenerate_black: int = 0x646464
    midnight_bluish: int = 0x303287
    dark_pastel_purple: int = 0x974376
    dark_goldenrod: int = 0xbd8c26
    dark_orange: int = 0xc5562a
    worn_leather: int = 0x8d6753
    blood_red: int = 0x880808
    purplish_red: int = 0x92113f
    thistle: int = 0xd8bfd8


class Icons():
    # Symbolic
    bell_orange: str = f"{CdnUri}{CdnImagesPath}/bell.png"  # generic notification
    pencil: str = 'https://cdn.discordapp.com/emojis/470326272401211415.png'  # generic modification
    checkmark_green: str = 'https://cdn.discordapp.com/emojis/470326274519334936.png'  # confirmation
    exclaim_red: str = 'https://cdn.discordapp.com/emojis/470326273298792469.png'  # exclamation point
    qmark_blue: str = 'https://cdn.discordapp.com/emojis/512367613339369475.png'  # question mark
    alarm_green: str = 'https://cdn.discordapp.com/emojis/477907607785570310.png'  # reminder added
    alarm_blue: str = 'https://cdn.discordapp.com/emojis/477907609215827968.png'  # reminder edit or sounding off
    alarm_red: str = 'https://cdn.discordapp.com/emojis/477907608057937930.png'  # reminder removed or expired
    star_red: str = 'https://cdn.discordapp.com/emojis/636288153044516874.png'
    star_green: str = 'https://cdn.discordapp.com/emojis/636288201258172446.png'
    hash_blue: str = 'https://cdn.discordapp.com/emojis/469950142942806017.png'  # invite detail
    hash_green: str = 'https://cdn.discordapp.com/emojis/469950144918585344.png'  # invite created
    hash_red: str = 'https://cdn.discordapp.com/emojis/469950145413251072.png'  # invite revoked or expired
    crown_blue: str = 'https://cdn.discordapp.com/emojis/469964153289965568.png'  # admin modification
    crown_green: str = 'https://cdn.discordapp.com/emojis/469964154719961088.png'  # admin confirmation
    crown_red: str = 'https://cdn.discordapp.com/emojis/469964154879344640.png'  # admin denial
    shield_red_deny: str = 'https://cdn.discordapp.com/emojis/472475292078964738.png'  # defcon action (defcon_denied)
    shield_red: str = 'https://cdn.discordapp.com/emojis/470326273952972810.png'  # defcon disarm (defcon_shutdown)
    shield_green_check: str = 'https://cdn.discordapp.com/emojis/470326274213150730.png'  # defcon armed (defcon_unshutdown)
    shield_blue_gear: str = 'https://cdn.discordapp.com/emojis/472472638342561793.png'  # decon policy change (defcon_update)
    shield_bug: str = f"{CdnUri}{CdnImagesPath}/shield_bug.png"  # bug reporting
    priority_trivial: str = f"{CdnUri}{CdnImagesPath}/trivial_priority.webp"
    priority_minor: str = f"{CdnUri}{CdnImagesPath}/minor_priority.webp"
    priority_lowest: str = f"{CdnUri}{CdnImagesPath}/lowest_priority.webp"
    priority_low: str = f"{CdnUri}{CdnImagesPath}/low_priority.webp"
    priority_medium: str = f"{CdnUri}{CdnImagesPath}/medium_priority.webp"
    priority_high: str = f"{CdnUri}{CdnImagesPath}/high_priority.webp"
    priority_highest: str = f"{CdnUri}{CdnImagesPath}/highest_priority.webp"
    priority_major: str = f"{CdnUri}{CdnImagesPath}/major_priority.webp"
    priority_critical: str = f"{CdnUri}{CdnImagesPath}/critical_priority.webp"
    priority_blocker: str = f"{CdnUri}{CdnImagesPath}/blocker_priority.webp"
    info_blue: str = f"{CdnUri}{CdnImagesPath}/info_blue.png"  # blue info icon outline
    info_grey: str = f"{CdnUri}{CdnImagesPath}/info_grey.png"  # light grey info icon outline
    deny_red: str = f"{CdnUri}{CdnImagesPath}/deny_red.png"  # red deny smybol outline
    db_error: str = f"{CdnUri}{CdnImagesPath}/database_delete.png"  # db icon with red x
    db_plus: str = f"{CdnUri}{CdnImagesPath}/database_add.png"  # db icon with green plus symbol
    db_check: str = f"{CdnUri}{CdnImagesPath}/database_verify.png"  # db icon with green checkmark
    db_download: str = f"{CdnUri}{CdnImagesPath}/database_download.png"  # db icon with green download icon
    bullhorn: str = f"{CdnUri}{CdnImagesPath}/megaphone.png"  # announcement
    brick_block: str = f"{CdnUri}{CdnImagesPath}/component_block.png"  # component
    gear_orange: str = f"{CdnUri}{CdnImagesPath}/settings_wheel_orange.png"  # setting update
    gear_grey: str = f"{CdnUri}{CdnImagesPath}/settings_wheel_grey.png"  # setting update
    crossed_tools: str = f"{CdnUri}{CdnImagesPath}/maintenance.png"  # setting update
    wrench: str = f"{CdnUri}{CdnImagesPath}/wrench.png"  # setting update
    bitcoin: str = f"{CdnUri}{CdnImagesPath}/btc_orange.png"  # btc symbol in orange
    firewall: str = f"{CdnUri}{CdnImagesPath}/firewall.png"
    outdent: str = f"{CdnUri}{CdnImagesPath}/outdent.png"
    pie_chart: str = f"{CdnUri}{CdnImagesPath}/pie_chart.png"
    ruler: str = f"{CdnUri}{CdnImagesPath}/orthogonal.png"
    code_brackets: str = f"{CdnUri}{CdnImagesPath}/code_brackets.png"

    # Chat
    user_join: str = 'https://cdn.discordapp.com/emojis/469952898181234698.png'
    user_part: str = 'https://cdn.discordapp.com/emojis/469952898089091082.png'
    user_ban: str = 'https://cdn.discordapp.com/emojis/469952898026045441.png'
    user_unban: str = 'https://cdn.discordapp.com/emojis/469952898692808704.png'
    user_mute: str = 'https://cdn.discordapp.com/emojis/472472640100106250.png'
    user_unmute: str = 'https://cdn.discordapp.com/emojis/472472639206719508.png'
    user_update: str = 'https://cdn.discordapp.com/emojis/469952898684551168.png'
    user_warn: str = 'https://cdn.discordapp.com/emojis/470326274238447633.png'
    message_bulk_delete: str = 'https://cdn.discordapp.com/emojis/469952898994929668.png'
    message_delete: str = 'https://cdn.discordapp.com/emojis/472472641320648704.png'
    message_edit: str = 'https://cdn.discordapp.com/emojis/472472638976163870.png'
    voice_state_blue: str = 'https://cdn.discordapp.com/emojis/656899769662439456.png'
    voice_state_green: str = 'https://cdn.discordapp.com/emojis/656899770094452754.png'
    voice_state_red: str = 'https://cdn.discordapp.com/emojis/656899769905709076.png'

    # Emoji
    emergency: str = 'https://cdn.discordapp.com/emojis/808420164081156196.gif'
    clown: str = f"{CdnUri}{CdnImagesPath}/clown-face_1f921.png"
    goblin: str = f"{CdnUri}{CdnImagesPath}/japanese-goblin_1f47a.png"
    honeypot: str = f"{CdnUri}{CdnImagesPath}/honeypot.png"
    eye_in_speech_bubble: str = f"{CdnUri}{CdnImagesPath}/eye_in_speech_bubble.png"
    warning: str = f"{CdnUri}{CdnImagesPath}/warning.png"

    # Fun
    retro_pc: str = f"{CdnUri}{CdnImagesPath}/retro_pc.gif"


class Emojis():
    attachment: str = "📎"
    link: str = "🔗"
    confirm: str = "✅"
    deny: str = "❌"
    cancel: str = "🚫"
    warning: str = "⚠"
    over_18: str = "🔞"
    speech_bubble: str = "💬"
    thinking: str = "💭"
    sauron: str = "👁️‍🗨️"
    eyes: str = "👀"
    ahegao: str = "🥵"
    no_gesture: str = "🙅‍♀️"
    voice: str = "🔉"
    speaker_muted: str = "🔇"
    mega: str = "📢"
    stand_mic: str = "🎙"
    hand_mic: str = "🎤"
    notes_on: str = "📳"
    notes_muted: str = "📵"
    departure: str = "🛫"
    arrival: str = "🛬"
    users: str = "👥"
    profile: str = "👤"
    thread: str = "🧵"
    idle: str = "💤"
    abacus: str = "🧮"
    brush: str = "🖌"
    cake: str = "🎂"
    calendar: str = "📅"
    sticker: str = "🥟"
    smile: str = "🙂"
    frown: str = "🙁"
    loading: str = "⏳"
    owner: str = "👑"
    wand: str = "🪄"
    janny: str = "🧹"
    rocket: str = "🚀"
    sparkle: str = "✨"
    cop: str = "👮"
    backpack: str = "🎒"
    magnify: str = "🔍"
    robot: str = "🤖"
    money_bag: str = "💰"
    knot: str = "🪢"
    neighborhood: str = "🏘"
    mammoth: str = "🦣"
    administrator: str = "🧑‍💼"
    hourglass: str = "⏳"
    pause: str = "⏸"
    skip: str = "⏩"
    ballot_box: str = "🗳"
    identification_card: str = "🪪"
    id_badge: str = identification_card
    wave: str = "👋"
    red_exclamation_mark: str = "❗"
    exclamation: str = red_exclamation_mark
    dumpling: str = "<:dumpling:1044603306305126452>"
    flatbread: str = "<:flatbread:1044604040811663421>"
    amulet: str = "<:amulet:1009960578116223006>"
    nitro: str = "<:nitro:1009682516808060958>"
    nitro_disabled: str = "<:nitro_disabled:1010687716066861168>"
    hypesquad: str = "<:hypesquad:1010684515695464539>"
    premium_tier: list = [
        None,
        "<:boosted_lvl_1:1044667024867074180>",
        "<:boosted_lvl_2:1044667023629746299>",
        "<:boosted_lvl_3:1044667022748950580>"
    ]
    priority_trivial: str = "<:priority_trivial:1044596054110248980>"
    priority_minor: str = "<:priority_minor:1044594899779067914>"
    priority_lowest: str = "<:priority_lowest:1044595192180768788>"
    priority_low: str = "<:priority_low:1044594897346363513>"
    priority_medium: str = "<:priority_medium:1044594898604662814>"
    priority_high: str = "<:high_priority:1044594896109056031>"
    priority_highest: str = "<:priority_highest:1044594894859149383>"
    priority_major: str = "<:priority_major:1044595193556516915>"
    priority_critical: str = "<:priority_critical:1044595190805049375>"
    priority_blocker: str = "<:priority_blocker:1044595189668397087>"


DefaultAvatars = [
    "e17e21e5e3760e7ad073e4f2c07811dc30f316b02953231fb19cd16f13a3996c",
    "b4b9ca3e260e49c62017a72b2ad75cd142b13f4ea0396e9e01dbd2cd11c4bf90",
    "7c0e3f31e27e0cfe92b34c1db147e2b6619d2d358ca6e224c9c9609fa3c85477",
    "7204bc8298380901d5d2d78c749cb8c08bb0d0bfcdb62e5f9e43197e1b24a8e5",
    "77732263513e8d974381536a75d980dec9ecec568712d10d043dd9b97a31f9e0",
    "5d8bff6e6c8a56d2dda65307cb66d4e81aa49d7b122ac99c62111dd5f6660460",
    "104ee6d39cca300a5de53d819805ee5d726ade99a44aa7c0aed3b83e8e82dd09",
    "268d3bff1c1a171084555c3275743da3c52e3240fc6cb96e15ef023ed96cd2b8"
]


class Symbols():
    mdash: str = "—"
    ldquo: str = "“"
    rdquo: str = "”"
    mid: str = "│"
    npc: str = "឵឵ ឵឵"


# https://github.com/Delitefully/DiscordLists/blob/master/flags.md
class UserFlags():
    mfa_sms_recovery: int = (1 << 4)
    partnership_pending: int = (1 << 11)
    unread_system_messages: int = (1 << 13)
    underage_blocked: int = (1 << 15)  # account pending deletion
    spammer: int = (1 << 20)
    nitro_disabled: int = (1 << 21)


class GuildNsfwFilter(Enum):
    disabled: str = Emojis.deny
    no_role: str = Emojis.confirm
    all_members: str = Emojis.over_18


class GuildVerificationLevel(Enum):
    none: str = Emojis.deny
    low: str = Emojis.priority_low
    medium: str = Emojis.priority_medium
    high: str = Emojis.priority_high
    highest: str = Emojis.priority_highest


class GuildNsfwLevel(Enum):
    default: str = "be unclassified"
    explicit: str = "be focused on explicit content"
    safe: str = "be safe for all ages"
    age_restricted: str = "have age-restricted content"


class GuildNoteLevel(Enum):
    all_messages: str = Emojis.notes_on
    only_mentions: str = Emojis.notes_muted


class BadTags():
    ehentai: tuple = (
        "female:lolicon",
        "female:oppai loli",
        "male:shotacon",
        "female:toddlercon",
        "male:toddlercon",
        "female:low lolicon",
        "male:low shotacon",
        "other:forbidden content",
    )
    nhentai: tuple = (
        "lolicon",
        "oppai loli",
        "shotacon",
        "low lolicon",
        "low shotacon",
        "forbidden content",
    )
    fragments: tuple = (
        "highschool",
        "high school",
        "middle school",
        "grade school",
        "years old",
        "year old",
        "lolicon",
        "pedophile",
        "preteen",
        "little girl"
        "little sister",
        "younger sister",
        "imouto",
        "little boy",
        "little brother",
        "younger brother",
        "otouto",
        "shougakkou",
        "chuugaku"
    )
    hydrus: tuple = (
        "straight shota",
        "older man and younger boy",
        "older man and younger girl",
        "teenage girl and younger boy",
        "teenage girl and younger girl",
        "loli with loli",
        "futa with loli",
        "sholicon",
        "shotadom",
        "shotasub",
        "underage sex",
        "human loli",
        "loli",
        "oppai loli",
        "lolidom",
        "underaged female",
        "shota",
        "human shota",
        "underaged male",
        "toddler",
        "toddlercon",
        "human cub",
        "underaged",
        "teenager"
        "age:teen",
        "high-school girl",
        "high school student",
        "middle schooler",
        "elementary school student",
        "age:preteen",
        "age:toddler",
        "child",
        "cub",
        "cubs",
        "cub porn",
        "cub sex",
        "cub/adult",
        "adult/cub",
        "foal",
        "foalcon",
        "implied foalcon",
        "young",
        "age:young",
        "aged down"
        "age:17",
        "age:16",
        "age:15",
        "age:14",
        "age:13",
        "12-year-old girl",
        "age:12",
        "age:11",
        "age:10",
        "age:9",
        "age:8",
        "age:7",
        "age:6",
        "age:5",
        "age:4",
        "age:3",
        "age:2",
        "randoseru",
        "crime prevention buzzer",
        "character:pokemon character",
    )
