import os
import base64
import logging
import nacl.utils
import nacl.secret
from cabling_bot.util.cryptography import CryptographyController

logger = logging.getLogger(__name__)


class CryptographyService():
    def __init__(
        self,
        bot,
        secrets
    ) -> CryptographyController:
        params = self._get_config(bot, secrets)

        self.controller = None

        if params["use_aws"]:
            source = "AWS Secrets Manager"
            _raw_material = secrets.get_secret_binary(
                f"cables/{params['environment']}/encryption_key_material",
                version_stage=params["key_version"]
            )
        elif params["key_path"]:
            source = self._check_key_path(params)
            if source == "mount":
                with open(params["key_path"], "wb") as f:
                    _raw_material = self._generate_key(params)
            elif source == "file":
                with open(params["key_path"], "rb") as f:
                    _raw_material = f.read()
        elif params["key_material"]:
            if params["environment"] != "development":
                e = "Refusing to load encryption key; env incompatible"
                raise RuntimeError(e)
            source = "config"
            _raw_material = base64.decodebytes(
                bytes(params["key_material"], "UTF-8")
            )

        if not _raw_material:
            raise RuntimeError("Failed to load encryption key")
        self.controller = CryptographyController(_raw_material)

        text = f"Encryption key read into memory from {source}"
        if source in ("file", "config"):
            logger.warning(text)
        else:
            logger.info(text)

    def _get_config(self, bot, secrets) -> dict:
        use_aws = True if secrets else False
        params = {
            "environment": bot.environment,
            "use_aws": use_aws,
            "key_path": bot.get_config("encryption.key_path", None),
            "key_material": bot.get_config("encryption.key_material", None),
            "key_version": bot.get_config(
                "aws.encryption_key_version",
                "AWSCURRENT"
            )
        }
        return params

    def _check_key_path(self, params: dict) -> str:
        _dir = os.path.dirname(params["key_path"])
        _exists = os.path.isfile(params["key_path"])
        if not _exists and os.path.ismount(_dir):
            return "mount"
        else:
            return "file"

    def _generate_key(self, params) -> bytes:
        with open(params["key_path"], "wb") as f:
            _raw_material = nacl.utils.random(
                nacl.secret.SecretBox.KEY_SIZE
            )
            f.write(_raw_material)
            return _raw_material
