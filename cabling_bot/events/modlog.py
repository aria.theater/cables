import logging

from datetime import (datetime, timedelta)
from typing import (Optional, Union)

from discord.ext import commands
from discord.utils import (format_dt, utcnow)

from ..util import matching
from ..util.members import get_first_join
from ..util.discord import get_system_channel
from ..util.transform import bool_to_emoji
from ..util.user import (safe_get, CustomUser)
from ..util.embeds import EmbedManager
from ..util.event_logging import EventLogController
from ..bot import constants

from discord import (
    Guild, Member, User, VoiceChannel, Thread,
    RawIntegrationDeleteEvent, Integration, BotIntegration,
    AuditLogAction, Embed, Colour,
)
from ..bot.types import StateData
from ..bot import CablesBot as Bot

logger = logging.getLogger(__name__)


class ModLog(commands.Cog):
    def __init__(self, bot: Bot, hidden=True) -> None:
        super().__init__()
        self._bot = bot
        self.controller = EventLogController(bot)
        self.log = EmbedManager(bot)

        bot.config_keys.new(
            "notifications",
            "guild_joins",
            "(bool) Send notifications when members join the server?",
            False
        )
        bot.config_keys.new(
            "notifications",
            "guild_parts",
            "(bool) Send notifications when members leave the server?",
            False
        )
        bot.config_keys.new(
            "notifications",
            "profile_updates",
            "(bool) Send notifications when Nitro members update their server nick and avatar?",
            False
        )
        bot.config_keys.new(
            "notifications",
            "vc_activity",
            "(list[int]) Send join and parts notifications for the given voice channels.",
            None
        )
        bot.config_keys.new(
            "notifications",
            "thread_watch_channels",
            "(list[int]) A list of Discord channel IDs to watch for and report on thread creation in",
            None
        )
        bot.config_keys.new(
            "notifications",
            "cables_kicks",
            ("(bool) Whether or not to report member kicks carried out by cables in an unattended fashion, "
             + "such as with scheduled unverified member purges. This doesn't effect purge summaries."),
            True
        )
        bot.config_keys.new(
            "notifications",
            "other_bot_kicks",
            "(bool) Whether or not to report member kicks mediated by bots other than cables",
            True
        )
        bot.config_keys.new(
            "notifications",
            "staff_kicks",
            "(bool) Whether or not to report member kicks by server staff, and/or with cables commands",
            True
        )

    @commands.Cog.listener("on_guild_member_join")
    async def handle_guild_join(
        self, member: Member, correlation: tuple, first_join: StateData
    ) -> None:
        """Log when a member joins a guild."""

        (member, route, invite, level) = correlation

        # NOTE: the join notification logic is heavily tied to the
        #   event log controller; hence, it resides there
        await self.controller.member_join(
            member, first_join,
            route, invite=invite,
            priority=level,
        )

    @commands.Cog.listener("on_guild_member_part")
    async def handle_guild_part(self, member: Member) -> None:
        """Log when a member voluntarily leaves a guild."""

        gid = member.guild.id
        if not self._bot.get_guild_key(gid, "notifications", "guild_parts"):
            return

        text = f"**Who:** {member.mention} (`{member}`)\n"

        rejoin = get_first_join(self._bot, member)
        diff = (rejoin.data - member.joined_at)
        if rejoin.state and diff < timedelta(days=1):
            label = "Last Join"
            text += f"**First Join:** {format_dt(rejoin.data, 'R')}\n"
        else:
            label = "Joined"
        text += f"**{label}:** {format_dt(member.joined_at, style='R')}\n"

        if member.top_role.name != "@everyone":
            text += f"**Top role:** `{member.top_role.name}`"

        await self.log.send(
            constants.Colours.soft_red,
            "User part", text,
            channel=get_system_channel(self._bot, member.guild),
            title_icon=constants.Icons.user_part,
        )

    @commands.Cog.listener("on_member_profile_update")
    async def handle_member_profile_update(self, before: Member, after: Member) -> None:
        """Log when a member with Nitro changes both their server avatar and display name."""

        #
        # NOTE:
        #   User bio is not accessible via the bot API
        # there's a /users/profile route, but it's undocumented
        # and only accessed by the client
        #   This only detects changes from Nitro users
        # because the Server Profile page doesn't support
        # changing the user profile avatar for non-Nitro users
        #

        gid = before.guild.id
        if not self._bot.get_guild_key(
            gid, "notifications", "profile_updates"
        ):
            return

        #
        # TODO:
        #   The Discord CDN deletes old assets, and sometimes
        #       _rather_ quickly.
        #   Unfortunately, `Embed.set_thumbnail`` doesn't support
        #       a file upload, though.
        #   We could mirror avatar assets at a website-backed
        #       S3 bucket, possibly as a stopgap until we have
        #       our own bot-integrated webserver.
        #

        # before_avatar_key = before.display_avatar.key
        # files.download_file(self._bot.tmp_path, before.display_avatar)
        # after_avatar_key = after.display_avatar.key
        # files.download_file(self._bot.tmp_path, after.display_avatar)

        old = Embed(title="Before", description=before.display_name)
        old.set_thumbnail(url=before.display_avatar)
        new = Embed(title="After", description=after.display_name)
        new.set_thumbnail(url=after.display_avatar)

        mention = f"{before.mention}{constants.Emojis.nitro}"
        await self.log.send(
            constants.Colours.greyblue,
            "Member profile revamp",
            f"{mention} changed both nickname and avatar",
            channel=get_system_channel(self._bot, after.guild),
            title_icon=constants.Icons.user_update,
            additional_embeds=[old, new]
        )

    #
    # NOTE: MEMBER_UPDATE events aren't accurately sent by the gateway.
    #           `before.name` and `after.name` are the same!
    #   https://gitlab.com/aria.theater/cables/-/issues/112#note_935333812
    #

    # @commands.Cog.listener("on_member_tag_update")
    # async def handle_member_tag_update(self, before: Member, after: Member) -> None:
    #     """Log when a member changes their own username component."""
    #     text = f"**Who:** {after.mention}\n"
    #     text += f"**Old:** `{before}`\n"
    #     text += f"**New:** `{after}`"
    #     await self.log.send(
    #         Colour.teal(),
    #         "Member tag changed",
    #         text,
    #         channel=after.guild.system_channel,
    #         title_icon=constants.Icons.user_update,
    #     )

    @commands.Cog.listener("on_member_timeout")
    async def handle_member_timeout(self, member: Member) -> None:
        """Log a moderator imposing timeout on a member."""

        expiry = format_dt(member.communication_disabled_until, style="R")

        guild = member.guild
        async for entry in guild.audit_logs(
            action=AuditLogAction.member_update,
            limit=1
        ):
            # in certain cases, the API continues to send this even when its expired
            # also, the date format is different between audit log entry and the event's member object 🙃
            audit_time = datetime.strptime(
                entry.after.communication_disabled_until,
                '%Y-%m-%dT%H:%M:%S.%f%z'
            )
            if audit_time != member.communication_disabled_until:
                break

            target, issuer = f"{member.mention} (`{member}`)", entry.user
            text = f"**Who:** {target}\n"
            text += f"**Issuer:** {issuer}\n"
            text += f"**Expiry:** {expiry}"
            if entry.reason:
                reason = entry.reason.replace("\n", "\n> ")
                text += f"\n**Reason:**\n> {reason}"

            await self.log.send(
                Colour.orange(),
                "User timeout", text,
                channel=get_system_channel(self._bot, guild),
                title_icon=constants.Icons.user_mute,
            )

    @commands.Cog.listener("on_member_kick")
    async def handle_member_kick(
        self,
        guild: Guild,
        target: Union[User, Member],
        issuer: Member,
        requestor: Optional[Member] = None,
        reason: Optional[str] = None,
    ) -> None:
        """Log guild member kick events."""

        cables_issued = bool(issuer.id == guild.me.id)
        by_cables = bool(
            self._bot.get_guild_key(
                guild.id, "notifications", "cables_kicks"
            ) and (cables_issued and not requestor)
        )
        by_another_bot = bool(
            self._bot.get_guild_key(
                guild.id, "notifications", "other_bot_kicks"
            ) and (not cables_issued and issuer.bot)
        )
        by_staff = bool(
            self._bot.get_guild_key(
                guild.id, "notifications", "staff_kicks"
            ) and (not issuer.bot
                   or (issuer.bot and requestor and cables_issued))
        )

        if (by_cables or by_another_bot or by_staff):
            await self.controller.member_sanction(
                guild, "kick",
                target, issuer, reason
            )

    @commands.Cog.listener("on_member_flagged_spammer")
    async def handle_member_flagged_spammer(self, member: Member) -> None:
        """Log a pre-existing member being flagged as a spammer."""
        await self.log.send(
            constants.Colours.midnight_bluish,
            "Member flag change",
            f"{member.mention} (`{member}`) has been flagged as a spammer!",
            channel=get_system_channel(self._bot, member.guild),
            title_icon=constants.Icons.user_update,
            mentionable="here"
        )

    @commands.Cog.listener("on_member_flagged_underage")
    async def handle_member_flagged_underage(self, member: Member) -> None:
        """Log a pre-existing member being flagged as underage.

        This occurs when they joined a server without an account \
            and fail the COPPA age gate during subsequent signup.
        """
        desc = (f"{member.mention} (`{member}`) was a temporary user who "
                + "admitted to being under the age of 13 during signup. "
                + "Their account has been disabled.")
        await self.log.send(
            constants.Colours.midnight_bluish,
            "Member flag change", desc,
            channel=get_system_channel(self._bot, member.guild),
            title_icon=constants.Icons.user_update
        )

    @commands.Cog.listener("on_guild_ownership_transfer")
    async def handle_guild_transfer(self, guild: Guild, before: Member, after: Member) -> None:
        """Log guild ownership transfers."""
        text = "Ownership of this guild has changed hands "
        text += f"from {before.mention} (`{before}`) to {after.mention} (`{after}`)"
        await self.log.send(
            constants.Colours.purplish_red,
            "Guild transfer", text,
            channel=get_system_channel(self._bot, guild),
            title_icon=constants.Icons.user_update,
            mentionable="everyone"
        )

    @commands.Cog.listener("on_guild_toggled_feature")
    async def handle_guild_toggled_feature(self, guild: Guild, feature: str, state: bool) -> None:
        """Log a guild enabling or disabling a specific feature."""
        state = "enabled" if state else "disabled"

        mentionable = None
        if feature == "COMMUNITY":
            if state and self._bot.environment == "production":
                mentionable = "here"
            name = "Community mode"
        elif feature == "NEWS":
            return
        else:
            name = feature

        await self.log.send(
            constants.Colours.dark_goldenrod,
            "Guild feature changed",
            f"{name} has been {state}",
            channel=get_system_channel(self._bot, guild),
            title_icon=constants.Icons.gear_grey,
            mentionable=mentionable
        )

    @commands.Cog.listener("on_integration_create")
    async def handle_integration_create(
        self,
        integration: Union[Integration, BotIntegration]
    ) -> None:
        """Log when a bot integrates with a guild."""

        # documentation is wrong; payload might be `BotIntegration`
        ## https://github.com/Pycord-Development/pycord/blob/06ac55b/discord/state.py#L1445-L1451
        ## https://github.com/Pycord-Development/pycord/blob/06ac55b/discord/integrations.py#L351-L364

        if type(integration) is not BotIntegration:
            return

        bot = integration.application.user
        verified = bool_to_emoji(bot.public_flags.verified_bot)
        text = (f"{bot.mention}{verified} integrated "
                + f"by {integration.user.mention}")

        await self.log.send(
            Colour.blue(),
            "Bot integrated", text,
            channel=get_system_channel(self._bot, integration.guild),
            title_icon=constants.Icons.pencil
        )

    @commands.Cog.listener("on_raw_integration_delete")
    async def handle_raw_integration_delete(
        self, payload: RawIntegrationDeleteEvent
    ) -> None:
        """Log when a bot de-integrates with a guild."""

        now = utcnow()
        guild = self._bot.get_guild(payload.guild_id)
        b: CustomUser = await safe_get(self._bot, payload.application_id)
        bot = b.user

        actor = "Unknown User"
        async for entry in guild.audit_logs(
            action=AuditLogAction.integration_delete,
            oldest_first=False,
            limit=3
        ):
            diff = abs((now - entry.created_at).seconds)
            if diff > 10:
                break
            if (
                isinstance(bot, User)
                and bot.id == entry.target.id
            ):
                actor = entry.user.mention

        if (
            b.mention == "Inaccessible Account"
            and actor == "Unknown User"
        ):
            text = f"Failed to discern de-integrated integration on {guild.id}"
            await self._bot.debug_channel.send(text + f"```{payload}```")
            return

        verified = str()
        if isinstance(bot, User):
            verified = bool_to_emoji(bot.public_flags.verified_bot)

        await self.log.send(
            Colour.dark_blue(),
            "Bot de-integrated",
            f"{bot.mention}{verified} de-integrated by `{actor}`",
            channel=get_system_channel(self._bot, guild),
            title_icon=constants.Icons.pencil
        )

    @commands.Cog.listener("on_guild_vc_join")
    async def handle_guild_vc_join(
        self, member: Member, channel: VoiceChannel
    ) -> None:
        """Log voice channel member join events.

        Only occurs if at least one person is already in the channel.
        """
        gid = channel.guild.id
        chids = self._bot.get_guild_key(gid, "notifications", "vc_activity")
        if not chids:
            return
        if matching.search_channels(channel, chids):
            text = f"Activity in 🔊 `{channel.name}`"
            channel = await member.guild.fetch_channel(channel.id)
            count = len(channel.members)
            if count < 2:
                return
            elif count == 2:
                text = (f"Session kicked off in 🔊 `{channel.name}`"
                        + f"by {channel.members[0].mention} "
                        + f"and {channel.members[1].mention}")
            else:
                text = f"{member.mention} joined 🔊 `{channel.name}`"
            await self.log.send(
                Colour.teal(),
                "Voice activity", text,
                channel=get_system_channel(self._bot, member.guild),
                title_icon=constants.Icons.voice_state_blue,
            )

    @commands.Cog.listener("on_guild_vc_part")
    async def handle_guild_vc_part(
        self, member: Member, channel: VoiceChannel
    ) -> None:
        """Log voice channel member leave events."""
        gid = channel.guild.id
        chids = self._bot.get_guild_key(gid, "notifications", "vc_activity")
        if not chids:
            return
        if channel.id in chids:
            await self.log.send(
                Colour.dark_teal(),
                "Voice activity",
                f"{member.mention} left 🔊 `{channel.name}`",
                channel=get_system_channel(self._bot, member.guild),
                title_icon=constants.Icons.voice_state_blue,
            )

    @commands.Cog.listener("on_thread_create")
    async def handle_thread_create(self, thread: Thread) -> None:
        """Notifies when a thread is created in watched channels."""

        gid = thread.guild.id
        chids = self._bot.get_guild_key(
            gid,
            "notifications", "thread_watch_channels"
        )
        if not chids:
            return

        if matching.search_channels(thread.parent, chids):
            author, parent = thread.owner, thread.parent
            text = f"[{thread.name}]({thread.jump_url}) has been created "
            text += f"by {author.mention} in {parent.mention}"
            await self.log.send(
                constants.Colours.soft_green,
                "Thread created", text,
                channel=get_system_channel(self._bot, thread.guild),
                title_icon=constants.Icons.hash_green
            )

    # NOTE: see these descriptions of Guild.nsfw_level
    #   Discord Developers, #api-questions:
    #      https://canary.discord.com/channels/613425648685547541/697489244649816084/916029111028310048
    #   Discord Documentation PR:
    #      https://github.com/discord/discord-api-docs/pull/3194
    @commands.Cog.listener("on_guild_update")
    async def handle_guild_update(self, before: Guild, after: Guild) -> None:
        if (
            before.nsfw_level != after.nsfw_level
            and not after.nsfw_level.default
        ):
            level = after.nsfw_level
            desc = (f"*{after.name}* has been reviewed by Discord service "
                    + "administrators and deemed to "
                    + f"{constants.GuildNsfwLevel[level.name].value}.")
            if level.age_restricted or level.explicit:
                desc += ("\n\nThis rating results in iOS users being unable "
                         + "to access your server.")
            if level.explicit:
                desc += (" However, your staff are still able to, by flipping "
                         + "the relevant toggle in their client settings.")
            guild = self._bot.get_guild(after)
            await self.log.send(
                constants.Colours.dark_goldenrod,
                "Server classified", desc,
                channel=get_system_channel(self._bot, guild),
                title_icon=constants.Icons.shield_red,
            )


def setup(bot: Bot):
    bot.add_cog(ModLog(bot))
