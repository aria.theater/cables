import logging
import json

from typing import Optional, Union
from discord.errors import (Forbidden, HTTPException)
from discord.ext.commands.errors import UserInputError
from discord import (Message, Embed, TextChannel, Thread, VoiceChannel)

from discord.ext import commands
from ..util import transform
from ..bot import (
    CablesContext as Context,
    CablesCog as Cog,
    CablesBot as Bot,
    checks
)

logger = logging.getLogger(__name__)


class EmbedCmdManager(Cog):
    """Create and manage rich media embeds. See `help embed` for more info."""

    def __init__(self, bot: Bot) -> None:
        super().__init__()
        self._bot = bot

    # NOTE: slash commands don't support attachments, nor linebreaks in option
    #   values. we cannot convert this module to interactions easily

    @commands.group()
    async def embed(self, ctx: Context):
        """Create and edit embed posts with JSON or inline markdown"""
        if ctx.invoked_subcommand is None:
            err = ("Invalid subcommand passed.\n"
                   + f"Try `{ctx.prefix}help embed` for more info.")
            await ctx.fail_msg(err)

    @embed.command(name="simple")
    async def simple_cmd(
        self, ctx: Context,
        title: str, description: str
    ) -> None:
        """Create a simple embed post with only a title and description"""
        embed = Embed(title=title, description=description)
        await ctx.react_success()
        await ctx.send(embed=embed)

    async def _get_user_json(
        self,
        msg: Message,
        raw_json: Optional[str]
    ) -> dict:
        """Grab the first attachment of a post and decode its json into a dict

        :raises UserInputError: Bad or missing json attachment
        """
        contents = None
        try:
            if msg.attachments and not raw_json:
                raw_contents = msg.attachments[0].read()
            else:
                raw_contents = raw_json
            contents = json.loads(raw_contents)
        except json.JSONDecodeError as e:
            err = ("Uploaded attachment not JSON or malformed!\n"
                   + transform.codeblock(e))
            raise UserInputError(err)
        else:
            return contents

    @embed.command(name="json")
    async def json_cmd(
        self, ctx: Context,
        *, raw_json: Optional[str]
    ) -> None:
        """Create an embed post from json

        Json information can be passed either as an attached file or inline.
        """
        try:
            contents = await self._get_user_json(ctx.message, raw_json)
            embed = Embed.from_dict(contents)
            await ctx.send(embed=embed)
        except UserInputError as e:
            await ctx.fail_msg(str(e))
        except HTTPException as e:
            err = "Failed to send embed:\n" + transform.codeblock(e)
            await ctx.fail_msg(err)
        else:
            await ctx.react_success()

    @checks.validate_arg_permissions(checks=["view", "read", "write"], index=2)
    @embed.command(name="edit")
    async def edit_cmd(
        self, ctx: Context,
        msg: Message,
        *, raw_json: Optional[str]
    ) -> None:
        """Edit an existing message embed with updated json information

        JSON can be passed either as an attached file or inline.
        """

        if len(msg.embeds) != 1:
            err = ("Messsage either has zero or multiple embeds, and I'm "
                   + "unfortuntaely not smart enough to handle editing posts "
                   + "with multiple embeds.")
            await ctx.fail_msg(err)
            return

        try:
            json = await self._get_user_json(ctx.message, raw_json)
            await msg.edit(embeds=[Embed.from_dict(json)])
        except UserInputError as e:
            await ctx.fail_msg(str(e))
        except HTTPException as e:
            err = "Failed to edit embed:\n" + transform.codeblock(e)
            await ctx.fail_msg(err)
        else:
            await ctx.react_success()

    @checks.validate_arg_permissions(checks=["view", "read"], index=2)
    @embed.command(name="dump")
    async def dump_cmd(self, ctx: Context, msg: Message) -> None:
        """Dump the JSON representation of the first embed in a message."""

        if len(msg.embeds) < 1:
            await ctx.fail_msg("Message doesn't have an embed.")
            return

        embed_json = json.dumps(msg.embeds[0].to_dict())
        jump_embed = Embed(description=f"[Jump to message]({msg.jump_url})")
        await ctx.reply(
            content=transform.codeblock(embed_json),
            embeds=[jump_embed]
        )

    @checks.validate_arg_permissions(checks=["view", "read"], index=2)
    @checks.validate_arg_permissions(checks=["view", "write"], index=3)
    @embed.command(name="copy", aliases=["cp"])
    async def copy_cmd(
        self, ctx: Context,
        msg: Message,
        channel: Union[Thread, VoiceChannel, TextChannel]
    ) -> None:
        """Copy an embed post to another channel or thread"""

        if len(msg.embeds) < 1:
            await ctx.fail_msg("Message doesn't have an embed.")
            return

        try:
            copy = await channel.send(embed=msg.embeds[0])
        except Forbidden:
            err = "Lacking send permissions in destination channel"
            await ctx.fail_msg(err)
            return
        if copy.channel != msg.channel:
            desc = f"[Jump to message]({copy.jump_url})"
            jump_embed = Embed(description=desc)
            await ctx.reply(
                content=f"Copied message to {channel}",
                embed=jump_embed
            )

        await ctx.react_success()


def setup(bot: Bot):
    bot.add_cog(EmbedCmdManager(bot))
