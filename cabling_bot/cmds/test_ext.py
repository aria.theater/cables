import logging
from typing import (Optional, Union, List)

from discord.ext import commands
from discord import (
    SlashCommandGroup, slash_command,
    Option, option,
    Guild, Member, Role,
    TextChannel, VoiceChannel,
    User, Asset, File, Embed,
    Forbidden, NotFound, InvalidArgument,
)

from cabling_bot.bot import (
    CablesCog as Cog,
    CablesBot as Bot,
    CablesContext as Context,
    CablesAppContext as ApplicationContext,
    CablesAutocompleteContext as AutocompleteContext,
    constants, checks, exceptions,
)
from cabling_bot.util import (
    embeds, interactions,
    transform, queue,
)

logger = logging.getLogger(__name__)

global CHOICES
CHOICES = [
    "anteater",
    "berry",
    "crustacean",
    "option"
]


## SINGLE SLASH COMMAND

# @slash_command(name="cmd", checks=[checks.is_app_owner().predicate])
# # @option("opt", description="An option", autocomplete=CHOICES)
# @option("opt", description="An option", choices=CHOICES)
# async def test_slash_cmd(ctx: ApplicationContext, opt: str):
#     await ctx.respond(f"You selected {opt}!")


## SLASH COMMAND GROUP

# test_group = SlashCommandGroup(
#     "test",
#     "Internal slash commands for bot testing purposes.",
#     guild_only=True,
#     checks=[checks.is_app_owner().predicate]
# )


# @test_group.command(name="cmd")
# @option("opt", description="An option", autocomplete=CHOICES)
# async def test_slash_cmd2(ctx: ApplicationContext, opt: str):
#     await ctx.respond(f"You selected {opt}!")


def setup(bot: Bot):
    pass
    # bot.add_application_command(test_group)
    # bot.add_application_command(test_slash_cmd)
