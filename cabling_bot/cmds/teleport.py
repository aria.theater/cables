import logging
import random

from typing import (Union, Optional)
from discord.ext import commands
from discord import (
    option, TextChannel, VoiceChannel,
    Message, WebhookMessage, Embed, Member
)

from ..bot import (
    CablesCog as Cog,
    CablesBot as Bot,
    CablesAppContext as ApplicationContext,
    constants,
)
from ..util import (embeds, channels)
from ..bot.types import MessageableChannel

logger = logging.getLogger(__name__)


class TeleportManager(Cog):
    """Direct users to move a conversation to another channel. See `help teleport` for more info."""

    def __init__(self, bot: Bot, hidden=True) -> None:
        super().__init__()

        self._bot = bot
        self.log = embeds.EmbedManager(bot)

    async def _craft_embed(
        self,
        channel: Union[TextChannel, VoiceChannel],
        msg: Union[WebhookMessage, Message],
        requestor: Member,
        topic: Optional[str] = None,
    ) -> Embed:
        direction = "To"
        if isinstance(msg, WebhookMessage):
            direction = "From"
        text = f"**{direction}:** `#{channel.name}`\n"
        embed = Embed(color=constants.Colours.steelblue, description=text)
        embed.set_author(name="Portal", url=msg.jump_url)

        embed.description += "**Reason:**\n> "
        if topic:
            embed.description += topic
        else:
            embed.description += f"Requested by {requestor.mention}"

        choice = random.choice(range(1, 6))
        extension = "webp"
        if choice in range(5, 6):
            extension = "gif"
        embed.set_thumbnail(url=(
            constants.CdnUri + constants.CdnImagesPath
            + "/portal" + str(choice) + "." + extension
        ))
        embed.set_footer(text="Protip: Tap 'Portal' for transport!")

        return embed

    @commands.slash_command(name="teleport", guild_only=True)
    @option(
        "channel", Union[TextChannel, VoiceChannel],
        description="Channel to teleport to",
        required=True,
    )
    @option(
        "topic", str,
        description="Topic/reason to move the conversation",
        required=False, default=None, max_length=1024,
    )
    async def teleport_cmd(
        self, ctx: ApplicationContext,
        channel: MessageableChannel,
        topic: str
    ) -> bool:
        """Direct users to move a conversation to another channel."""
        await ctx.response.defer()

        err = None
        if not (
            await channels.is_channel_sendable(ctx.bot, ctx.channel)
            and await channels.is_channel_sendable(ctx.bot, channel)
        ):
            err = (f"{constants.Emojis.deny} Channel permissions "
                   + "prevent opening a portal.")
        elif not (
            ctx.channel.permissions_for(ctx.author).send_messages
            and channel.permissions_for(ctx.author).send_messages
        ):
            err = (f"{constants.Emojis.deny} Channel permissions "
                   + "prevent you from posting at the destination.")
        if err:
            await ctx.followup.send(err)
            return False

        text = f"{constants.Emojis.hourglass} Portal to {channel.mention} opening..."
        origin_msg = await ctx.followup.send(text, wait=True)
        text = f"{constants.Emojis.hourglass} Portal to {ctx.channel.mention} opening..."
        destination_msg = await channel.send(text)

        origin_embed = await self._craft_embed(
            channel, destination_msg, ctx.author, topic=topic
        )
        destination_embed = await self._craft_embed(
            ctx.channel, origin_msg, ctx.author, topic=topic
        )
        await ctx.followup.edit_message(origin_msg.id, content=None, embeds=[origin_embed])
        await destination_msg.edit(None, embeds=[destination_embed])

        return True


def setup(bot: Bot):
    bot.add_cog(TeleportManager(bot))
