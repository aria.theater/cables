import logging
import asyncio
import psycopg_pool

from typing import (Optional, Union, Sequence, NamedTuple)
from psycopg import DatabaseError
from discord.utils import (utcnow, find)
from discord.ext import commands

from ..util.privileged import is_staff_member
from ..util.user import has_default_avatar
from ..bot.errors import report_database_error, report_guild_error
from ..util.event_logging import EventLogController
from ..util.transform import truncate_text

from discord import (
    Guild, Role, Member,
    SlashCommandGroup, option,
    Embed, Webhook, Interaction,
    TextChannel, Thread,
)
from discord.errors import (Forbidden, HTTPException)
from ..bot import (
    CablesCog as Cog,
    CablesBot as Bot,
    CablesAppContext as ApplicationContext,
    checks, constants
)

logger = logging.getLogger(__name__)
mdash = constants.Symbols.mdash


class MemberVerificationManager(Cog):
    def __init__(self, bot: Bot, hidden=True) -> None:
        super().__init__()
        self._bot = bot
        self.log = EventLogController(bot)
        self.db: psycopg_pool.ConnectionPool = bot.db
        q = """\
            -- Exclusion list
            CREATE TABLE IF NOT EXISTS member_autopurge_exclusions(
                id              serial      PRIMARY KEY,
                guild           bigint      NOT NULL,
                member          bigint      NOT NULL,
                created_at      timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP,
                created_by      bigint      NOT NULL
            );
            CREATE UNIQUE INDEX IF NOT EXISTS idx_mapexclusions_guildmember
                ON member_autopurge_exclusions (guild, member);
            """
        with self.db.connection() as conn:
            try:
                conn.execute(q)
            except DatabaseError as e:
                logger.debug("Cog init error: " + e)

        self.module = "member_verification"
        bot.config_keys.new(
            "feature_flags", self.module,
            "(bool) Whether or not the member verification module is enabled",
            False
        )

        self.default_approval_policies = ["pfp"]
        self.default_scan_policies = ["nick", "pfp"]
        self.default_ticket_prefices = ["ticket", "onhold", "approved", "mismatch", "rejected"]

        # Ticket bot integration
        bot.config_keys.new(
            self.module, "ticket_channel_prefix",
            "(str) Common prefix for all ticket channel names e.g. 'ticket-'",
            "ticket-"
        )

        # Approvals
        bot.config_keys.new(
            self.module, "verified_role",
            "(int) Role which is given to members approved through the application process",
            None
        )
        bot.config_keys.new(
            self.module, "auto_role",
            "(int) Role which is given automatically on join",
            None
        )
        bot.config_keys.new(
            self.module, "pending_role",
            "(int) Role which is granted to those applying for membership",
            None
        )
        bot.config_keys.new(
            self.module, "min_elected_roles",
            ("(int) Minimum number of user-elected roles that are required "
             + "for the system to allow a moderator to approve them."),
            0
        )
        bot.config_keys.new(
            self.module, "application_time_limit",
            ("(int) Length in days which unverified members are alotted to "
             + "complete the application process"),
            3
        )
        # bot.config_keys.new(
        #     self.module, "approval_policies",
        #     ("(list[str]) Which pre-qualification checks for the policy "
        #      + "engine to run as part of the approval workflow."),
        #     str(self.default_approval_policies)
        # )
        bot.config_keys.new(
            self.module, "orientation_channels",
            ("(list[dict]) A list of dictionaries with channel ID (id) and "
             + "description (note) e.g. [{id=1234567890,note='for conduct "
             + "guidelines'},...]"),
            None
        )

        # Purges
        bot.config_keys.new(
            self.module, "purge_summaries",
            ("(bool) Whether or not to notify staff of unverified member "
             + "purge outcomes each time the cronjob fires"),
            False
        )

        # Scanning
        bot.config_keys.new(
            self.module, "scan_entrants",
            ("(bool) Employ decisions by the policy engine against entrants. "
             + "Requires the member verification module."),
            False
        )
        bot.config_keys.new(
            self.module, "scan_policies",
            ("(list[str]) Which pre-qualification checks for the policy "
             + "engine to run as part of the approval workflow."),
            str(self.default_scan_policies)
        )

    def enabled(self, guild: Guild) -> bool:
        return self._bot.get_guild_key(
            guild.id,
            "feature_flags", "member_verification"
        )

    cmd_group = SlashCommandGroup(
        "verifications",
        "Setting management and commands for the member verification module",
        guild_only=True,
    )

    ## PURGE

    async def _get_purge_config(
        self, guild: Union[Guild, int]
    ) -> tuple[Guild, Role, int]:
        if type(guild) is int:
            guild = self._bot.get_guild(guild)

        role_id = self._bot.get_guild_key(
            guild.id, self.module, "verified_role"
        )
        if not (role := guild.get_role(role_id)):
            raise TypeError
        time_limit = self._bot.get_guild_key(
            guild.id, self.module, "application_time_limit"
        )

        return (guild, role, time_limit)

    def _get_allow_list(
        self, guild: Guild,
    ) -> Sequence[NamedTuple]:
        q = """
            SELECT member
            FROM member_autopurge_exclusions AS acl
            WHERE acl.guild = %(guild)s;
        """
        with self._bot.db.connection() as conn:
            cursor = conn.execute(q, dict(guild=guild.id))
            if not cursor.rowcount:
                return list()
            return cursor.fetchall()

    def _populate_queue(
        self,
        guild: Guild, role: Role, time_limit: int
    ) -> Optional[list[Member]]:
        now = utcnow()
        exclusions = [row.member for row in self._get_allow_list(guild)]
        queue = [m for m in guild.members if
                 # Role check
                 role not in m.roles
                 # Membership length check
                 and (now - m.joined_at).days >= time_limit
                 # ACL check
                 and m.id not in exclusions
                 # Staff check
                 and not is_staff_member(self._bot, m)
                 # Bot check
                 and not m.bot]
        if not queue:
            return []
        return queue

    async def purge(
        self,
        guild: Guild, role: Role,
        time_limit: int,
        notify: Optional[bool] = False,
        dry_run: bool = False,
    ) -> Optional[Union[tuple[int, int], list[Member]]]:
        if not (q := self._populate_queue(guild, role, time_limit)):
            return None
        if dry_run:
            return q

        total, i = len(q), 0
        kick_failed, dm_failed = 0, 0
        for m in q:
            membership_length = (utcnow() - m.joined_at).days
            dm_text = (
                f"You have been auto-removed from *{guild.name}*.\n\n"
                + "A membership application wasn't fully completed within the "
                + f"{membership_length} days since you'd joined.\n\nYou're "
                + "welcome to rejoin when you have the time, interest and "
                + "energy to finish.\n\nIf you have any feedback or would "
                + "like to make an accommodation request, please reach out to "
                + f"{guild.owner.mention}!"
            )

            try:
                await m.send(dm_text)
            except (Forbidden, HTTPException):
                dm_failed += 1
            try:
                await m.kick(reason="Auto-purge unverified member")
            except (Forbidden, HTTPException):
                kick_failed += 1

            i += 1
            await asyncio.sleep(2.5)

        if (
            notify
            or self._bot.get_guild_key(
                guild.id, self.module, "purge_summaries"
            )
        ):
            counts = (total, dm_failed, kick_failed)
            await self.log.purge_summary(guild, counts)

        return (total, (dm_failed + kick_failed))

    @checks.is_admin()
    @checks.is_feature_enabled("member_verification")
    @cmd_group.command(name="purge")
    @option("time_limit", int, description="Days since member joined", default=None)
    @option("dry_run", bool, description="Simulate what would happen?", default=False)
    async def purge_cmd(
        self, ctx: ApplicationContext,
        time_limit: Optional[int] = None,
        dry_run: Optional[bool] = False,
    ) -> None:
        try:
            config = await self._get_purge_config(ctx.guild)
        except TypeError:
            err = "❌ Verification role isn't set."
            await ctx.respond(err, ephemeral=True)
            return
        if not time_limit:
            guild, role, time_limit = config
        else:
            guild, role, _ = config

        if not dry_run:
            await ctx.response.defer(ephemeral=True)

        result = await self.purge(
            guild, role, time_limit,
            notify=True,
            dry_run=dry_run,
        )

        if dry_run:
            if not result:
                await ctx.respond("Nothing to do. 🧐")
                return
            members = "\n".join([f"{mdash} {m.mention}" for m in result])
            text = (f"Would kick `{len(result)}` members idle for "
                    + f"`{time_limit}` day(s):\n{members}")
            embed = Embed(description=truncate_text(text, limit=2000))
            await ctx.respond(embeds=[embed])
        else:
            await ctx.followup.send(
                f"{constants.Emojis.confirm} Done!",
                ephemeral=True
            )

    @commands.Cog.listener("on_member_purge")
    async def cron_job(self) -> None:
        q = """
            SELECT guild FROM guild_cron_jobs
            WHERE "name" = 'member_autopurge' AND "enabled" = 'true';
        """
        with self.db.connection() as conn:
            cursor = conn.execute(q)
            if not cursor.rowcount:
                return
            targets = cursor.fetchall()

        for target in targets:
            start = utcnow()

            if not (
                self.enabled(self._bot.get_guild(target.guild))
                and (config := await self._get_purge_config(target.guild))
            ):
                continue

            response = await self.purge(*config)
            runtime = (utcnow() - start).seconds
            if not response:
                return

            total, failed = response
            log = (f"Purged {total - failed} unverified members from "
                   + f"{config[0].id} in {runtime} secs")
            logger.debug(log)

            await asyncio.sleep(60)

    ## APPROVE

    def _collect_roles(self, ctx: ApplicationContext) -> tuple[
        Optional[list[Role]], Optional[list[Role]]
    ]:
        gid = ctx.guild.id
        removed_roles, added_roles = [], []
        if (auto_role := ctx.get_guild_key(gid, self.module, "auto_role")):
            removed_roles += [ctx.guild.get_role(auto_role)]
        if (pending_role := ctx.get_guild_key(gid, self.module, "pending_role")):
            removed_roles += [ctx.guild.get_role(pending_role)]
        if (verified_role := ctx.get_guild_key(gid, self.module, "verified_role")):
            added_roles += [ctx.guild.get_role(verified_role)]

        if ctx.guild.id == 992464815958269962:
            added_roles += [
                # administrative role
                ctx.guild.get_role(1022897478288289885),
                # color role
                ctx.guild.get_role(1017918879378460754),
                # divider roles
                ctx.guild.get_role(1021094383124222083),
                ctx.guild.get_role(1021094418507382887),
                ctx.guild.get_role(1021094450107256873),
                ctx.guild.get_role(1021094484047564820)
            ]

        return added_roles, removed_roles

    async def _can_approve(self, ctx: ApplicationContext, member: Member) -> bool:
        gid = ctx.guild.id
        conditions = []
        min_roles = ctx.get_guild_key(gid, self.module, "min_elected_roles")
        if min_roles and len(member.roles) < int(min_roles):
            conditions += ["take additional tags"]
        pfp_required = ctx.get_guild_key(gid, self.module, "pfp_required")
        if pfp_required and await has_default_avatar(ctx.bot, member):
            conditions += ["set a profile picture"]
        if conditions:
            conditions = " and ".join(conditions)
            await ctx.followup.send(f"❌ Policy requires member {conditions}.")
            return False
        return True

    async def _send_approval_notice(
        self, ix: Union[Webhook, Interaction], member: Member
    ) -> None:
        desc = ("We sincerely appreciate your cooperation with, and "
                + "participation in, our application process~")

        guild = member.guild
        if (channels := self.get_guild_key(
            guild.id, self.module, "orientation_channels"
        )):
            count = 1
            desc += ("\n\nWhen you get some time, please read over these "
                     + "resources to help orient yourself to our space:\n\n")
            for f in channels:
                if not guild.get_channel_or_thread(f["id"]):
                    count += 1
                    continue
                desc += f"**{count}.** See <#{f['id']}> for {f['note']}.\n\n"
                count += 1

        embed = Embed(title="Notice of approval")
        embed.description = desc
        embed.colour = constants.Colours.dim_green
        embed.set_author(name="Application decision",
                         icon_url=constants.Icons.shield_green_check)
        await ix.send(embeds=[embed])

    def is_ticket_channel(
        self,
        channel: Union[Thread, TextChannel],
        member: Member,
    ) -> bool:
        gid = channel.guild.id
        prefix = self.get_guild_key(gid, self.module, "ticket_channel_prefix")
        if not (
            (channel.name.startswith(prefix)
                or channel.name.split("-")[0] in self.default_ticket_prefices)
            and ((erole := channel.changed_roles[0])
                 and erole.name == "@everyone"
                 and not erole.permissions.view_channel)
            and ((overwrite := channel.overwrites.get(member, None))
                 and overwrite.view_channel)
        ):
            return False
        return True

    async def rename_ticket(
        self, channel: Union[Thread, TextChannel], status: str
    ) -> Optional[bool]:
        if not channel.permissions_for(channel.guild.me).manage_channels:
            return None
        username = "-".join(channel.name.split("-")[1:])
        channel_name = f"{status}-{username}"
        if channel_name == channel.name:
            return False
        else:
            await channel.edit(name=channel_name)
            return True

    @commands.check_any(
        checks.is_global_mod(),
        checks.is_admin()
    )
    @checks.is_feature_enabled("member_verification")
    @cmd_group.command(name="approve")
    @option("member", Member, description="Who to approve")
    async def approve_cmd(
        self, ctx: ApplicationContext,
        member: Member
    ) -> None:
        channel = ctx.channel
        await ctx.response.defer()

        added_roles, removed_roles = self._collect_roles(ctx)
        if not added_roles:
            await ctx.followup.send("❌ Verification role isn't set.")
            return
        if not (await self._can_approve(ctx, member)):
            return

        reason = f"Approved by {ctx.author}"
        try:
            await member.add_roles(*added_roles, reason=reason)
            await member.remove_roles(*removed_roles, reason=reason)
        except Forbidden:
            err = "❌ Issue updating some roles."
            await ctx.followup.send(err)
            return

        if self.is_ticket_channel(channel, member):
            await self._send_approval_notice(ctx.followup, member)
            await self.rename_ticket(channel, "approved")

        await self.log.member_approved(ctx, member)

    ## PURGE EXCEPTIONS

    def _db_add_purge_exception(
        self,
        guild: Guild,
        target: Member,
        requestor: Member
    ) -> None:
        """Exclude an unverified member from being auto-purged according to the time limit."""

        sql = """
            INSERT INTO member_autopurge_exclusions
                (guild, member, created_by)
            VALUES
                (%s, %s, %s);
        """
        values = (guild.id, target.id, requestor.id)
        with self.db.connection() as conn:
            conn.execute(sql, values)

    @commands.check_any(
        checks.is_global_mod(),
        checks.is_admin()
    )
    @checks.is_feature_enabled("member_verification")
    @cmd_group.command(name="extend")
    @option("member", Member, description="Who to add an auto-purge exception for")
    async def add_purge_exception_cmd(
        self, ctx: ApplicationContext,
        member: Member
    ) -> None:
        """Extend interview timer, excluding a member from auto-purge."""
        exceptions = [row.member for row in self._get_allow_list(ctx.guild)]
        if member.id in exceptions:
            err = (f"{constants.Emojis.deny} `{member}` is "
                   + "already immune to auto-purge")
            await ctx.respond(err, ephemeral=True)
            return

        try:
            self._db_add_purge_exception(ctx.guild, member, ctx.author)
        except DatabaseError as e:
            await report_database_error(
                self._bot, ctx,
                "add auto-purge exception", e
            )
            return
        else:
            await ctx.respond(f"{constants.Emojis.confirm} Done!")

    def _db_get_purge_exception(
        self,
        guild: Guild,
        target: Member,
    ) -> Optional[bool]:
        """Check for an auto-purge exclusion existing for a given member."""

        sql = """
            SELECT true FROM member_autopurge_exclusions
            WHERE guild = %s AND member = %s
        """
        values = (guild.id, target.id)
        with self.db.connection() as conn:
            try:
                cursor = conn.execute(sql, values)
            except DatabaseError:
                return None
            if not cursor.rowcount:
                return False
            return True

    def _db_remove_purge_exception(
        self,
        guild: Guild,
        target: Member,
    ) -> bool:
        """Remove an auto-purge exclusion for a given member."""

        sql = """
            DELETE FROM member_autopurge_exclusions
            WHERE guild = %s AND member = %s
        """
        values = (guild.id, target.id)
        with self.db.connection() as conn:
            cursor = conn.execute(sql, values)
            if not cursor.rowcount:
                return False
            return True

    @commands.Cog.listener("on_autopurge_exception_removal")
    async def handle_exception_removal_on_part(self, member: Member) -> None:
        try:
            self._db_remove_purge_exception(member.guild, member)
        except DatabaseError as e:
            await report_database_error(
                self._bot, None,
                "auto-removing auto-purge exclusion", e
            )

    ## MEMBER JOIN SCANNING

    async def _has_bad_nick(self, member: Member) -> bool:
        guild = member.guild

        violate = None
        undead = ("Deleted User" in member.name)
        spammer = (".gg/" in member.name)
        if undead or spammer:
            if undead:
                reason = "Undead"
            elif spammer:
                reason = "Potential spammer"
            violate = True
        if not violate:
            return False

        text = (f"{constants.Emojis.deny} You have been automatically "
                + f"rejected from _{guild.name}_ by security policy:\n"
                + f"> {reason}")
        try:
            await member.send(text)
        except Forbidden:
            pass

        self._bot.dispatch("reject_entrant", "kick", f"DEFCON: {reason}")
        return True

    async def _has_bad_pfp(self, member: Member) -> bool:
        guild = member.guild
        if not await has_default_avatar(self._bot, member):
            return True
        text = (f"{constants.Emojis.warning} _{guild.name}_ requires a "
                + "profile picture be set.")
        try:
            await member.send(text)
        except Forbidden:
            m = member.mention
            err = f"Failed to notify {m} of non-default avatar policy."
            await report_guild_error(self._bot, None, None, guild, err)
        return False

    @commands.Cog.listener("on_assign_auto_role")
    async def handle_auto_role(self, member: Member, role: Role) -> None:
        try:
            await member.add_roles(role)
        except Forbidden:
            guild_config = self.init_guild_config(self.db, member.guild.id)
            guild_config.unset(self.module, "auto_role")
            cmd = (f"{self._bot.command_prefix}config guild set {self.module} "
                   + f"auto_role {role.id}")
            perm = "Lacking `Manage Roles` permission."
            err = (f"{constants.Emojis.deny} Auto-role assignment  disabled: "
                   + f"{perm}. Fix this, then re-configure with `{cmd}`")
            await report_guild_error(self._bot, None, None, member.guild, err)

    @commands.Cog.listener("on_reject_entrant")
    async def handle_entrant_rejection(
        self, member: Member, action: str, reason: str
    ) -> None:
        if action == "kick":
            await member.kick(reason=reason)
        elif action == "ban":
            # TODO: tie into ban manager
            await member.ban(reason=reason, delete_message_days=0)

    @commands.Cog.listener("on_verify_member_join")
    async def handle_join_verification(self, member: Member) -> None:
        """Runs checks and actions based on policy against an entrant."""
        guild, gid = member.guild, member.guild.id
        guild_config = self.init_guild_config(self.db, gid)

        if self._bot.get_guild_key(gid, self.module, "scan_entrants"):
            violate, invalid = [], []
            for cond in self.get_guild_key(gid, self.module, "scan_policies"):
                if cond not in self.default_scan_policies:
                    invalid += [cond]
                    continue
                method = getattr(self, f"_has_bad_{cond}")
                violate += [await (method)(member)]
            if invalid:
                guild_config.set(self.module, "scan_entrants", False)
                guild_config.set(self.module, "scan_policies", self.default_scan_policies)
                err = (f"{constants.Emojis.warning} Entrant scanning has been "
                       + f"disabled and reset. Invalid policies: {invalid}")
                await report_guild_error(self._bot, None, None, guild, err)
            if any(violate):
                # NOTE: log violations?
                pass

        if (rid := self._bot.get_guild_key(gid, self.module, "auto_role")):
            role = member.guild.get_role(rid)
            if not role:
                guild_config.unset(self.module, "auto_role")
                err = (f"{constants.Emojis.deny} Auto-role assignment "
                       + "disabled; role doesn't exist.")
                await report_guild_error(self._bot, None, None, guild, err)
            else:
                self._bot.dispatch("assign_auto_role", member, role)


def setup(bot: Bot):
    bot.add_cog(MemberVerificationManager(bot))
