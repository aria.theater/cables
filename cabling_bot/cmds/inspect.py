import logging
from typing import (Optional, Union)
from discord import (Embed, Forbidden, Member, User, Guild)
from discord.embeds import EmptyEmbed
from cabling_bot.bot import (
    CablesCog as Cog,
    CablesBot as Bot,
    CablesContext as Context,
    checks
)

from discord.ext import commands
from discord.utils import format_dt

import cabling_bot.util.user
import cabling_bot.util.discord
from cabling_bot.bot import constants
from cabling_bot.bot.errors import is_privileged_invoke
from cabling_bot.util import (embeds, transform)
from cabling_bot.util.channels import (
    get_voice_channels,
    get_text_channels,
    get_threads,
)

logger = logging.getLogger(__name__)


class InspectManager(Cog):
    """Commands for power users and the paranoid. See `help inspect` for more info."""

    def __init__(self, bot: Bot) -> None:
        super().__init__()
        self._bot = bot

    @commands.group(aliases=[])
    async def inspect(self, ctx: Context):
        """Commands for power users and the paranoid."""
        if ctx.invoked_subcommand is None:
            await ctx.fail_msg("Invalid command passed")

    # TODO: convert to embed, using fields
    @inspect.command(aliases=["u"])
    async def user(self, ctx: Context, user: Optional[Union[Member, User]] = None) -> None:
        """Inspect a Discord user account."""
        if not user:
            user = ctx.author
        text = ""

        acct_age, age_warning = format_dt(user.created_at, 'R'), str()
        if not cabling_bot.util.user.account_age_check(ctx.bot, user, ctx.guild):
            age_warning += constants.Emojis.warning
        text += f"**Account Birth:** {acct_age} {age_warning}\n"

        badges = cabling_bot.util.user.collect_badges(user)
        if badges:
            text += f"**Sash:** {' '.join(badges)}\n"

        # privileged: bot operators only
        if await is_privileged_invoke(ctx):
            mutual_guilds = user.mutual_guilds
            if mutual_guilds:
                text += "**Guilds:**\n"
            for guild in mutual_guilds:
                attributes = cabling_bot.util.discord.guild_attributes(
                    guild, user
                )
                text += (f"{constants.Symbols.mdash} "
                         + f"__{guild.name}__ ({guild.id})")
                tab = "     "
                if any(attributes.values()):
                    text += f"\n{tab}*Sash:*"
                    for v in attributes.values():
                        text += f" {v}" if v else ""
                created_at = format_dt(guild.created_at, "R")
                text += f"\n{tab}*Created:* {created_at}"
                joined_at = format_dt(guild.me.joined_at, "R")
                text += f"\n{tab}*Joined:* {joined_at}\n"

        await ctx.reply(text, mention_author=False)

    async def _privileged_guild_fields(self, guild: Guild) -> list[dict]:
        loading = "⏳ Loading"

        admins = cabling_bot.util.discord.get_administrators(guild)
        jannies = cabling_bot.util.discord.get_janitors(guild)
        leadership = (
            f"👑 {guild.owner.mention}\n"
            + (f"🪄 `{len(admins) - 1}` Admins\n"
                if len(admins) > 1 else "")
            + (f"🧹 `{len(jannies)}` Jannies\n"
                if len(jannies) > 1 else "")
        )

        emoji_num, emoji_max = len(guild.emojis), guild.emoji_limit
        are_emojis_hap = (guild.emojis and emoji_num < emoji_max)
        emoji_ico = ("🙂" if are_emojis_hap else "🙁")
        sticker_num, sticker_max = len(guild.stickers), guild.sticker_limit
        are_stickers_hap = (guild.stickers and sticker_num < sticker_max)
        sticker_ico = (constants.Emojis.dumpling if are_stickers_hap
                       else constants.Emojis.flatbread)
        assets = (f"""{emoji_ico} `{emoji_num}` (`{emoji_max}`) Emoji
                  {sticker_ico} `{sticker_num}` (`{sticker_max}`) Stickers""")

        channels = str()
        text_channels = get_text_channels(guild)
        if text_channels:
            channels += f"💬 `{len(text_channels)}` Text\n"
        nsfw_channels = [c for c in text_channels if c.is_nsfw()]
        if nsfw_channels:
            channels += f"🔞 `{len(nsfw_channels)}` R-18\n"
        if guild.voice_channels or guild.stage_channels:
            voice_channels = len(get_voice_channels(guild))
            channels += f"🔉 `{voice_channels}` Voice\n"

        tup = cabling_bot.util.discord.get_member_breakdown(guild)
        bots, humans, ratio = tup
        bot_farm = cabling_bot.util.discord.has_excessive_bots(
            bots, humans, ratio
        )
        online = len(cabling_bot.util.discord.list_members(
            guild, constants.UserStatuses.online
        ))
        # present = len(cabling_bot.util.discord.list_members(
        #     guild, constants.UserStatuses.present
        # ))
        # idle = len(cabling_bot.util.discord.list_members(
        #     guild, constants.UserStatuses.afk
        # ))

        members = (f"👥 `{online}` (`{humans}`)\n"
                   # + f"🟢 `{present}` Online\n"
                   # + f"🟠 `{idle}` AFK\n"
                   + f"🤖 `{bots}` Bots"
                   + (" " + constants.Emojis.warning
                      if bot_farm else "")
                   + "\n")

        if guild.premium_subscribers:
            members += f"🚀 `{guild.premium_subscription_count}` Boosters\n"

        prune_estimate = 0
        if not guild.unavailable:
            try:
                prune_estimate = await guild.estimate_pruned_members(days=30)
            except Forbidden:
                pass
        if prune_estimate:
            members += f"💤 `{prune_estimate}` Prunable\n"

        roles = str()
        colored_roles = [r for r in guild.roles if r.colour.value]
        roles = (
            (f"🖌 `{len(colored_roles)}` Colored\n" if colored_roles else "")
            + (f"🧮 `{len(guild.roles)}` Total")
        )

        settings = await cabling_bot.util.discord.get_guild_settings(guild)
        (notes, uav_level, content_filter, mfa_level, widget_state) = settings
        content_filter = constants.GuildNsfwFilter[content_filter]
        uav_level = constants.GuildVerificationLevel[uav_level]
        staff_mfa = transform.bool_to_emoji(bool(mfa_level))
        widget = transform.bool_to_emoji(bool(widget_state))
        notes = constants.GuildNoteLevel[notes]
        settings = f"""
            {notes.value} Default Notifs.
            {uav_level.value} Verification Lvl.
            {content_filter.value} R-18 Filter
            {staff_mfa} Staff MFA
            {widget} Widget
        """

        # NOTE: API returns the vanity url but this isn't exposed by the library
        #   also, we hit the field limit, so adding that would require restructuring
        return [
            # dict(name="Owner", value=guild.owner.mention),
            # dict(name="Admins", value=admins),
            dict(name="Leadership", value=leadership),
            dict(name="Assets", value=assets),
            dict(name="Members", value=(members + loading)),
            dict(name="Channels", value=(channels + loading)),
            dict(name="Roles", value=roles),
            dict(name="Settings", value=settings),
        ]

    @checks.is_guild_available()
    @inspect.command(aliases=["g", "s", "server"])
    async def guild(self, ctx: Context, guild: Optional[Guild] = None) -> None:
        """Inspect a Discord guild that the bot is a member of.

        For non-privileged users, they too must be a member."""

        fields = []
        if not guild:
            guild = ctx.guild

        reply = await ctx.reply(constants.Emojis.loading, mention_author=False)

        name, footer_note = guild.name, str()
        if guild.unavailable:
            name = constants.Emojis.warning + " " + name
            footer_note = (f"{constants.Emojis.exclamation} Service outage "
                           + "is impacting guild availability")
        if guild.premium_tier:
            name += " " + constants.Emojis.premium_tier[guild.premium_tier]

        footer_text, footer_icon = str(), EmptyEmbed
        jump_text = f" ([jump]({guild.jump_url}))"
        _desc = self._bot.get_guild_key(guild.id, "general", "description")
        if (desc := _desc or guild.description or str()):
            desc = transform.add_fancy_quotes(desc, italicize=True)
            footer_text += f"{constants.Emojis.id_badge}  {guild.id}"
            if guild.id != ctx.guild.id:
                desc += jump_text
            offset = 3
        else:
            value = f"{constants.Emojis.id_badge} {str(guild.id)}{jump_text}"
            fields += [dict(name="ID", value=value, inline=False)]
            offset = 4

        created_at = format_dt(guild.created_at, "R")
        joined_at = format_dt(guild.me.joined_at, "R")
        dates = (f"🎂 {created_at}\n"
                 + f"🛬 {joined_at}")
        fields += [dict(name="Dates", value=dates)]

        # TODO: get the color from the guild's icon
        embed = Embed(title=name, description=desc)
        if guild.icon:
            embed.set_thumbnail(url=guild.icon.url)

        has_auth = await is_privileged_invoke(ctx, guild=guild)
        if has_auth:
            fields += await self._privileged_guild_fields(guild)

        for f in fields:
            try:
                inline = f["inline"]
            except KeyError:
                inline = True
            embed.add_field(
                name=f["name"],
                value=f["value"],
                inline=inline,
            )

        features = cabling_bot.util.discord.guild_attributes(guild)
        features = " ".join([a for a in features.values() if a])
        if features and has_auth:
            embed.insert_field_at(
                index=offset + 4,
                name="Features", value=features
            )
        elif features and not has_auth:
            embed.add_field(name="Features", value=features)

        ## NOTE: this shrinks the embed dimensions
        ##       while causing field text to overflow,
        ##       and the whole thing just looks jank 😩
        # if guild.banner:
        #     embed.set_image(url=guild.banner.url)

        footer_parts = list()
        if footer_text:
            footer_parts += [footer_text]
        if footer_note:
            footer_parts += [footer_note]
        if footer_parts:
            footer_text = transform.format_embed_footer(footer_parts)
            embed.set_footer(text=footer_text, icon_url=footer_icon)

        # post initial data
        await reply.edit(content=None, embed=embed)
        if has_auth:
            # wait for remote calls, then update again
            loading = "⏳ Loading"

            members = embeds.get_field_value(embed, "Members")
            members = members.removesuffix(loading)
            if not guild.unavailable:
                bans = await cabling_bot.util.discord.list_bans(guild)
                members += f"🚫 `{len(bans)}` Bans\n" if bans else ""
                embed.set_field_at(index=offset, name="Members", value=members)
            await reply.edit(content=None, embed=embed)

            channels = embeds.get_field_value(embed, "Channels")
            channels = channels.removesuffix(loading)
            if (
                not guild.unavailable
                and (threads := await get_threads(guild))
            ):
                (_, threads, _) = threads
                channels += f"🧵 `{len(threads)}` Threads\n"
                embed.set_field_at(index=offset + 1,
                                   name="Channels", value=channels)
            await reply.edit(content=None, embed=embed)


def setup(bot: Bot):
    bot.add_cog(InspectManager(bot))
