#!/bin/bash

_BOLD=$(tput bold)
_NORMAL=$(tput sgr0)
function _log () {
	local _NOW; _NOW="$(date -Is)"
  local _LEVEL
	test "$1" = 'info' && _LEVEL=INFO
	test "$1" = 'error' && _LEVEL=ERROR
  test "$1" = 'warn' && _LEVEL=WARNING
	test ! "$_LEVEL" && _LEVEL="$1"

	if [ "$_LEVEL" = 'ERROR' ]
		then printf '%s\t%s\t%s%s%s\n' "$_NOW" "$_LEVEL" "${_BOLD}" "$2" "${_NORMAL}" >&2
		else printf '%s\t%s\t%s%s%s\n' "$_NOW" "$_LEVEL" "${_BOLD}" "$2" "${_NORMAL}"
  fi
}
export _log

function _usage () {
    _log error "$1"
    exit 64
}
export _usage

function _depfail () {
    _log error "Requires $1, but not found"
    exit 78
}
export _depfail
