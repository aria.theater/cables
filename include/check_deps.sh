#!/bin/bash

# TODO: update to check for >= 3.9
_PYVER=$(python3 -V)
[ $? = '127' ] && _PYVER=$(python -V)
if [ "${_PYVER:0:8}" != "Python 3" ]; then
  _depfail 'python >= 3.x'; fi

if [ ! "$(command -v poetry)" ]; then
  _depfail 'Poetry'; fi
if [ ! "$(command -v jq)" ]; then
  _depfail 'jq'; fi
if [ ! "$(command -v grep)" ]; then
  _depfail 'grep'; fi
