#!/bin/bash -e

# Capture optional arguments
while [[ "$1" =~ ^-- ]]; do
    # Select AWS variant of app compose file
    [ "$1" = '--aws' ] && { APP_COMPOSE_FILE=infra/docker-compose.aws.yml; }

    # Set docker-compose profile
    [[ "$1" =~ ^--profile= ]] && { COMPOSE_PROFILE=$1; }

    shift
done

# Set default docker-compose profile if necessary
[ ! "$APP_COMPOSE_FILE" ] && { APP_COMPOSE_FILE=infra/docker-compose.yml; }

# Usage
if  [[ ! "$1" || ! "$1" =~ (clean|build|up|down|restart) ]] ||
    [[ "$1" = restart && ! "$2" ]]
then
    echo 'Usage: $0 [--aws] clean|build|up|down|restart [service-name]' >&2
    exit 64
fi

# Constant `docker-compose` argument
# to set build context as root project directory
BASE_ARGS=("--project-directory=$(dirname $0)")
APP_ARGS=(${BASE_ARGS[*]} "$COMPOSE_PROFILE")

# Command parsing & control flow
CMD=$1; shift
case $CMD in
    clean)
        docker container prune -f
        docker rmi $(docker images -f "dangling=true" -q)
        ;;
    build)
        docker-compose ${BASE_ARGS[*]} -f infra/docker-compose.base.yml build
        docker-compose ${APP_ARGS[*]} -f "$APP_COMPOSE_FILE" build
        ;;
    up)
        docker-compose ${APP_ARGS[*]} -f "$APP_COMPOSE_FILE" up -d $@
        ;;
    down)
        docker-compose ${APP_ARGS[*]} -f "$APP_COMPOSE_FILE" down $@
        ;;
    restart)
        docker-compose ${APP_ARGS[*]} -f "$APP_COMPOSE_FILE" restart $@
        ;;
    deploy)
        APP_VERSION_NOW=$(git name-rev HEAD@{0} --name-only)
        git fetch --quiet
        APP_VERSION_LATEST=$(git name-rev deploy --name-only)
        if [ "$APP_VERSION_NOW" == "$APP_VERSION_LATEST" ]; then
            echo 'No upgrade pending'
            exit 0
        fi

        GIT_STATUS=$(git status --porcelain)
        if [ "$GIT_STATUS" ]; then
            echo 'Stashing your modifications'
            git stash push -m "Changes to $APP_VERSION_NOW"
        fi

        GIT_BRANCH=$(git branch --show-current)
        if [ "$GIT_BRANCH" != "deploy" ]; then
            git checkout deploy --quiet
        fi

        echo "Pulling $APP_VERSION_LATEST"
        git pull --quiet

        DOCKER_IMAGES_DANGLING="$(docker images -f "dangling=true" -q)"
        if [ "$DOCKER_IMAGES_DANGLING" ]; then
            echo 'Cleaning up Docker images'
            "$0" clean
        fi

        echo 'Building images'
        "$0" build

        echo "Deploying $APP_VERSION_LATEST"
        "$0" up $@

        # TODO: rollback
        ;;
esac
