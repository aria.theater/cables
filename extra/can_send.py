import logging
from typing import Optional
from discord import (Forbidden, NotFound, User, DMChannel)
from cabling_bot.bot import CablesContext as Context

logger = logging.getLogger(__name__)


async def can_send_dm(ctx: Context, user: User) -> Optional[bool]:
    """
    Test case for `discord.Object.can_send()` invoked on the following oobject types:
    - `discord.User`
    - `discord.Member`
    - `discord.DMChannel`

    Definition: https://github.com/Pycord-Development/pycord/blob/06ac55b/discord/abc.py#L1651-L1659
    """

    if not (user and hasattr(ctx, "author")):
        return False

    dm_channel = None
    try:
        assert user.dm_channel
        dm_channel = await ctx.bot.fetch_channel(user.dm_channel)
    except (NotFound, AssertionError):
        pass
    try:
        if not dm_channel:
            dm_channel: DMChannel = await ctx.bot.create_dm(user)
        _dm_sendable = dm_channel.can_send()
    except (NotFound, Forbidden):
        return False

    _user_id = user.id
    user = await ctx.bot.fetch_user(_user_id)
    _user_sendable = user.can_send()

    member = ctx.author
    if member:
        _member_sendable = member.can_send()

    if not (_dm_sendable or _user_sendable or _member_sendable):
        # reply = ("Temporarily change your privacy settings to allow DMs "
        #         + "from server members, and then try again.")
        # await ctx.fail_msg(reply)
        logger.debug("can_send_dm: can_send() returned falsey in all cases")
        return False
