from discord import Message
from discord.errors import (Forbidden, HTTPException)

chids = []
for chid in chids:
    channel = ctx.guild.get_channel(chid)
    async for msg in channel.history(limit=None, oldest_first=False):
        msg: Message
        if (
            msg.type.name not in ("default", "reply")
            or msg.webhook_id
            or msg.flags.is_crossposted
            or not msg.attachments
        ):
            continue

        try:
            await msg.delete()
        except (Forbidden, HTTPException):
            await ctx.channel.send(f"Failed to delete message (`{msg.id}`)")
            continue

        text = f"💣 **__Message Deleted__**\n\n"
        text += f"**ID:** `{msg.id}`\n"
        text += f"**Poster:** {msg.author.mention} (`{msg.author}`)\n"
        text += f"**Channel:** {channel.mention}\n"
        acount = len(msg.attachments)
        atypes = [t.content_type for t in msg.attachments]
        text += f"**Attachments:** **{acount}** of type(s): {atypes}\n"
        await ctx.channel.send(text)