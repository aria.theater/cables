from discord import (Message, Guild)
from discord.errors import (NotFound, Forbidden)

for guild in ctx.bot.guilds:
    guild: Guild
    members = await guild.fetch_members(limit=0).flatten()

q = """
    SELECT "user" FROM guild_member_joins
    WHERE guild = %s
    GROUP BY "user"
"""
v = (ctx.guild.id,)
with ctx.bot.db.connection() as conn:
    cur = conn.execute(q, v)
    joins = cur.fetchall()

targets = list()
for uid in joins:
    user = ctx.guild.get_member(uid.user)
    if not user:
        user = ctx.bot.get_user(uid.user)
    if not user:
        continue
    if user in targets:
        continue
    targets += [user]

text = f"You are receiving this message as you are or were a member of _{ctx.guild.name}_. "
text += "This server will cease operations within 24 hours, and subsequently be deleted.\n\n"
text += "The operator has produced user data archives, which are available upon request.\n\n"
text += "If you took a link to a piece of media uploaded to this server by doing 'copy link' or somesuch, and posted it elsewhere, those links will no longer be valid once the server is deleted. This may not happen immediately due to the asynchronous nature of Discord server deletions.\n\n"
text += f"Please contact {ctx.guild.owner.mention} (`{ctx.guild.owner}`) for any inquiry, be it to request a copy of your data, for your data to be _removed_ from the archive, or otherwise."

# await ctx.channel.send(embeds=[Embed(description=f"Would dispatch DMs to: `{[u.name for u in targets]}`")])
for user in targets:
    msg: Message = await ctx.channel.send(f"Dispatching DM to {user.mention} (`{user}`)")
    try:
        await user.send(text)
    except (NotFound, Forbidden):
        await msg.add_reaction("❌")
    else:
        await msg.add_reaction("✅")
