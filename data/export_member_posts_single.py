import os
from discord.utils import format_dt
from discord import (Thread, Forbidden)
from cabling_bot.util.permissions import is_channel_readable
from cabling_bot.util.statistics import get_targets
from cabling_bot.util.files import (download_file, delete_file)

mid: int  # set this
member = ctx.guild.get_member(mid)
mstr = member.display_name.lower().replace(" ", "_")
fd_prefix = f"/media/ssd-array/backups/chats/discord/user_requests/{mstr}"

channels = get_targets(ctx.bot, ctx.guild)
all_threads: list[Thread] = list()
for c in channels:
    all_threads += c.threads
    try:
        async for t in c.archived_threads(limit=None, private=False):
            all_threads += [t]
        async for t in c.archived_threads(limit=None, private=True):
            all_threads += [t]
    except Forbidden:
        pass
targets = channels + all_threads

msgs = dict()
for t in targets:
    if not is_channel_readable(t.permissions_for(ctx.guild.me)):
        continue

    await ctx.channel.send(f"Scanning {t.mention}...")
    async for m in t.history(
        after=member.joined_at,
        limit=None,
        oldest_first=True
    ):
        if (
            m.type.name not in ("default", "reply")
            or m.author.bot
            or m.webhook_id
            or m.flags.is_crossposted
        ):
            continue

        if not t.id in msgs:
            msgs[t.id] = list()
        if m.author.id == mid:
            msgs[t.id] += [m]

    if t.id in msgs and (count := len(msgs[t.id])):
        await ctx.channel.send(f"Exporting **{count}** matches from {t.mention}...")
        if isinstance(t, Thread):
            tname = f"{t.parent.name} - {t.name}"
        else:
            tname = t.name
        fd_path = f"{fd_prefix}/{tname}.txt"
        if os.path.exists(fd_path):
            delete_file(fd_path)
        with open(fd_path, "w+") as fd:
            for m in msgs[t.id]:
                created_at = m.created_at.strftime("%Y-%m-%d %I:%M:%S %p")
                text = f"[{created_at}] {m.author}\n\n"
                if m.content:
                    text += m.content + "\n\n"
                if m.attachments:
                    for a in m.attachments:
                        try:
                            fname = download_file(fd_prefix, a.url)
                            spath = f"{fd_prefix}/{a.filename}"
                            if not os.path.exists(spath):
                                os.rename(f"{fd_prefix}/{fname}", spath)
                            else:
                                fext = a.filename.split(".")[-1]
                                spath = f"{fd_prefix}/{fname}.{fext}"
                                os.rename(f"{fd_prefix}/{fname}", spath)
                        except FileNotFoundError:
                            pass
                        finally:
                            sname = spath.split("/")[-1]
                            text += f"[Attached File '{sname}']\n"
                    text += "\n"
                fd.write(text)
    else:
        await ctx.channel.send(f"No matches from {t.mention} to export")

text = "Done!"
if (count := len(msgs)):
    text += f" Exported messages from **{count}** targets!"
await ctx.channel.send(text)