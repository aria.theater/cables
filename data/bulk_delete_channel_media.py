from datetime import datetime
from dateutil.relativedelta import relativedelta
from discord import (Message, TextChannel)
from discord.errors import (Forbidden, HTTPException)

chids = []  # set this
candidates = dict()
for chid in chids:
    channel = ctx.guild.get_channel(chid)
    async for msg in channel.history(
        after=(datetime.now() - relativedelta(days=-14)),
        limit=None,
        oldest_first=False
    ):
        msg: Message
        if (
            msg.type.name not in ("default", "reply")
            or msg.webhook_id
            or msg.flags.is_crossposted
            or not msg.attachments
        ):
            continue
        if msg.channel not in candidates:
            candidates[msg.channel] = list()
        candidates[msg.channel] += [msg]

for channel, msgs in candidates.items():
    c, targets = 0, list()
    for msg in msgs:
        c += 1
        if c < 100:
            targets += [msg]
            continue

    channel: TextChannel
    try:
        await channel.delete_messages(targets)
    except (Forbidden, HTTPException):
        err = f"Batch message deletion in {channel.mention} failed!\n"
        err += "```" + ", ".join([m.id for m in msgs]) + "```"
        await ctx.channel.send(err)
        continue

    text = f"💣 **__Messages Deleted__**\n"
    text += f"**Channel:** {channel.mention}\n"
    acount = len([msg.attachments for msg in msgs])
    text += f"**Count:** {acount}\n"
    asize = sum([a.size for msg in msgs for a in msg.attachments])
    text += f"**Data Sum:** {asize}"
    await ctx.channel.send(text)
