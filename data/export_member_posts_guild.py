import os
import json
from datetime import datetime
from discord import (Thread, Forbidden)
from cabling_bot.util.permissions import is_channel_readable
from cabling_bot.util.statistics import get_targets
from cabling_bot.util.files import (download_file, delete_file)
from cabling_bot.util.user import safe_get


now = datetime.now().strftime("%Y-%m-%d")
lpath = "/media/ssd-array/backups/chats/discord/user_requests"

try:
    assert (
        getattr(self, "data")
        and self.data[now]
        and len(self.data[now])
    )
except AttributeError:
    self.data = {f"{now}": {}}

    channels = get_targets(ctx.bot, ctx.guild)
    threads = list()
    for c in channels:
        threads += c.threads
        try:
            async for t in c.archived_threads(limit=None, private=False):
                threads += [t]
            async for t in c.archived_threads(limit=None, private=True):
                threads += [t]
        except Forbidden:
            pass
    targets = channels + threads

    for t in targets:
        if not is_channel_readable(t.permissions_for(ctx.guild.me)):
            continue

        async for msg in t.history(limit=None, oldest_first=True):
            if (
                msg.type.name not in ("default", "reply")
                or msg.webhook_id
                or msg.flags.is_crossposted
            ):
                continue

            if t.id not in self.data[now]:
                self.data[now][t.id] = dict()
            if msg.author.id not in self.data[now][t.id]:
                self.data[now][t.id][msg.author.id] = list()
            self.data[now][t.id][msg.author.id] += [msg]

for target, member_posts in self.data[now].items():
    if not (t := ctx.guild.get_channel_or_thread(target)):
        continue
    tname = f"{t.parent.name} - {t.name}" if isinstance(t, Thread) else t.name
    for mid, msgs in member_posts.items():
        member = await safe_get(ctx.bot, mid, ctx.guild)
        if member.user is None:
            continue
        mid = member.user.id

        fd_prefix = f"{lpath}/{member.user.display_name}"
        if not os.path.exists(fd_prefix):
            os.mkdir(fd_prefix)
        fd_path = f"{fd_prefix}/{tname}.txt"

        for msg in msgs:
            mode = "a" if os.path.exists(fd_path) else "w"
            with open(fd_path, mode) as fd:
                created_at = msg.created_at.strftime("%Y-%m-%d %I:%M:%S %p")
                text = f"[{created_at}] {msg.author}\n\n"
                if msg.content:
                    text += msg.content + "\n\n"
                if msg.attachments:
                    for a in msg.attachments:
                        try:
                            fname = download_file(fd_prefix, a.url)
                            spath = f"{fd_prefix}/{fname}"
                            dpath = f"{fd_prefix}/{a.filename}"
                            if not os.path.exists(dpath):
                                os.rename(spath, dpath)
                            else:
                                fext = a.filename.split(".")[-1]
                                spath = f"{fd_prefix}/{fname}.{fext}"
                                if os.path.exists(dpath):
                                    break
                                os.rename(f"{fd_prefix}/{fname}", dpath)
                            delete_file(spath)
                        except FileNotFoundError:
                            pass
                        finally:
                            sname = spath.split("/")[-1]
                            text += f"[Attachment {sname}]\n"
                    text += "\n"
                fd.write(text)
