from cabling_bot.util.statistics import get_targets
from discord.errors import (Forbidden, NotFound, HTTPException)

mid: int = 0
member = await ctx.guild.fetch_member(mid)
msgs = list()
channels = get_targets(self._bot, ctx.guild)
for c in channels:
    async for msg in c.history(
        limit=None,
        after=member.joined_at,
        oldest_first=True
    ):
        if msg.author.id == mid:
            msgs += [msg]

# text = f"Would delete **{len(msgs)}** messages"
# await ctx.channel.send(text)

f = []
for m in msgs:
    try:
        await m.delete(reason="Requested by author")
    except (Forbidden, NotFound, HTTPException):
        f += [m]

s = len(msgs) - len(f)
text = f"Deleted **{s}** / {len(msgs)} messages successfully\n\n"
text += "*Failures:*" + "\n".join([f"{m.channel.id}/{m.id}" for m in f]) if f else ""
await ctx.channel.send(text)
